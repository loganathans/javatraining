package training;

import java.util.HashSet;
import java.util.Set;

public class ImmutableEmployee {
		private final String name;
		private final String id;
		private final String designation;
		private Set<String> skillSet;

		public ImmutableEmployee(String name, String id, String designation, Set<String> skillSet) {
			super();
			this.name = name;
			this.id = id;
			this.designation = designation;
			this.skillSet = new HashSet<>(skillSet); // Defensive copy
		}

		public String getName() {
			return name;
		}

		public String getId() {
			return id;
		}

		public String getDesignation() {
			return designation;
		}

		@Override
		public String toString() {
			return "ImmutableEmployee [name=" + name + ", id=" + id + ", designation=" + designation + ", skill-set=" + skillSet +"]";
		}
		
		public String[] getSkillSet() {
			return skillSet.toArray(new String[skillSet.size()]); // Defensive copy
		}
	}