package training;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Employee implements Cloneable, Comparable<Employee> {
	private final String name;
	private final String id;
	private String designation;
	private Set<String> skillSet = new HashSet<>();

	public Employee(String name, String id) {
		super();
//		System.out.println("Creating employee object:" + name);
		this.name = name;
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public String getId() {
		return id;
	}

	public String getDesignation() {
		return designation;
	}

	public Employee setDesignation(String designation) {
		this.designation = designation;
		return this;
	}

	@Override
	public String toString() {
		return "\nEmployee [name=" + name + ", id=" + id + ", designation=" + designation + ", skill-set=" + skillSet +"]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		System.out.println("Hashcode method invoked...: " + result);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		System.out.println("Equals method invoked...");
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public Employee clone() throws CloneNotSupportedException {
		System.out.println("Cloning employee object:" + this.name);
		Employee clone = (Employee) super.clone();
		clone.setSkillSet(new HashSet<>(this.getSkillSet())); // Deep cloning
		return clone;
	}

	@Override
	public int compareTo(Employee other) {
		System.out.println("Comareto method invoked...");
		// Comparing by employee id
		return this.id.compareToIgnoreCase(other.id);
	}
	
	public Set<String> getSkillSet() {
		return skillSet;
	}

	public void setSkillSet(Set<String> skillSet) {
		this.skillSet = skillSet;
	}

	public static void main(String[] args) {
		List<Employee> employees = new ArrayList<Employee>();
		employees.add(new Employee("Carol", "C001").setDesignation("Developer"));
		employees.add(new Employee("Alice", "C002").setDesignation("Tester"));
		employees.add(new Employee("Esther", "C003").setDesignation("Tester"));
		employees.add(new Employee("David", "C004").setDesignation("Admin"));
		employees.add(new Employee("Bob", "C005").setDesignation("Developer"));

		System.out.println("\nEmployees list:\n" + employees);

		System.out.println("\nTesting Equals Method");
		// Equals method Test;
		Employee test = new Employee("Esther", "C006");
		System.out.println(employees.contains(test)); // result: false

		test = new Employee("Anilin", "C003");
		System.out.println(employees.contains(test));// result: true

		System.out.println("\nTesting Hashcode Method");
		// Hashcode Method test
		HashMap<Employee, String> empolyeesDesignationTable = new HashMap<>();
		for (Employee employee : employees) {
			empolyeesDesignationTable.put(employee, employee.getDesignation());
		}

		System.out.println("\nEmployees Table:\n" + empolyeesDesignationTable);

		// Hashcode and Equals method Test;
		test = new Employee("Esther", "C006");
		System.out.println(empolyeesDesignationTable.containsKey(test));

		test = new Employee("Anilin", "C003");
		if (empolyeesDesignationTable.containsKey(test)) {
			System.out.println("The designation of " + test + " is " + empolyeesDesignationTable.get(test));
		}

		// Clone test
		// Cloning all employees
		List<Employee> clonedEmployees = new ArrayList<Employee>();
		for (Employee employee : employees) {
			try {
				clonedEmployees.add(employee.clone());
			} catch (CloneNotSupportedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		System.out.println("Cloned employeos:\n" + clonedEmployees);

		System.out.println("CompareTo method test");
		Collections.sort(clonedEmployees);

		System.out.println("ID wise Sorted Employees: \n" + clonedEmployees);

		System.out.println("Comparator test");
		Collections.sort(clonedEmployees, new Comparator<Employee>() {
			@Override
			public int compare(Employee emp1, Employee emp2) {
				System.out.println("Comparator#compare() method invoked..");
				return emp1.getName().compareTo(emp2.getName());
			}
		});

		System.out.println("Name wise Sorted Employees: \n" + clonedEmployees);

	}

}
