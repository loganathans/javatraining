package training;

public class InitializationTest {
	public static void main(String[] args) {
		System.out.println(">>>>>>>>Inside main...");
		System.out.println("################Instantiating X...");
		InitializationClass x = new InitializationClass();
		System.out.println("###Printing X value..");
		x.printVals();
		System.out.println("###End of Printing X value.\n");

		System.out.println("################Instantiating Y...");
		InitializationClass y = new InitializationClass();
		System.out.println("###Printing Y value..");
		y.printVals();
		System.out.println("###End of Printing Y value.\n");
		
		System.out.println("################Instantiating Y...");
		InitializationClass z = new SubInitializationClass();
		System.out.println("###Printing Z value..");
		z.printVals();
		System.out.println("###End of Printing Z value.\n");
		System.out.println(">>>>>>>>End of Main.\n");
	}
}

class InitializationClass {
	{
		// Instance Initializer 1
		System.out.println("########Start of Instance Initialization...");
		System.out.println("###Printing instance values at the start...");
		printInstanceVals();
	}

	static {
		// Static Initializer 1
		System.out.println("########Start of Static Initialization...");
		System.out.println("###Printing static values at the start...");
		printStaticVals();
	}

	int val = 10;
	int val1 = val + 10;

	public InitializationClass() {
		System.out.println("##Inside constructor...");
		val3 = val2 + 10;

		System.out.println("##End of constructor.\n");
	}
	
	public InitializationClass(int val) {
		System.out.println("##Inside parametarized constructor...");
		val3 = val2 + val;
		System.out.println("##End of parametarized constructor...");
	}

	int val2 = val1 + constant;
	int val3;

	static int sVal = 100;
	static int sVal1 = sVal + 100;
	// private static int sVal2 = sVal1 + constant; Not allowed

	static final int constant = 1000;
	static final int constant2;
	static int sVal2 = sVal1 + constant;
	static int sVal3 = sVal2 + 100;

	static {
		// Static Initializer 2
		constant2 = constant + 1000;
		System.out.println("###Printing static values at the end.");
		printStaticVals();
		System.out.println("########End of Static Initialization.\n");
	}

	{
		// Instance Initializer 2
		System.out.println("###Printing instance values at the end.");
		printInstanceVals();
		System.out.println("########End of Instance Initialization.\n");
	}

	void printVals() {
		printInstanceVals();
		printStaticVals();
	}

	private void printInstanceVals() {
		System.out.println("val = " + val);
		System.out.println("val1= " + val1);
		System.out.println("val2= " + val2);
		System.out.println("val3= " + val3);
	}

	private static void printStaticVals() {
		System.out.println("sVal = " + sVal);
		System.out.println("sVal1= " + sVal1);
		System.out.println("sVal2= " + sVal2);
		System.out.println("sVal3= " + sVal3);
		System.out.println("constant = " + constant);
		System.out.println("constant2= " + constant2);
	}

}

class SubInitializationClass extends InitializationClass {
	
}

