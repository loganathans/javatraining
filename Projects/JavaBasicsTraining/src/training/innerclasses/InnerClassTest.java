package training.innerclasses;

public class InnerClassTest {
	public static class OuterClass {

		private String instanceStr;
		private static String staticeStr = "Static String";
		private static InnerStaticClass innerStaticClassInstance = new InnerStaticClass("Static String");
		private InnerInstanceClass innerInstanceClassInstance;

		public OuterClass(String instanceStr) {
			this.instanceStr = instanceStr;
			innerInstanceClassInstance = new InnerInstanceClass();
		}

		void print() {
			innerStaticClassInstance.print();
			innerInstanceClassInstance.print();
		}

		static class InnerStaticClass {
			private String str;
			
			public InnerStaticClass() {
				this("Default Inner Static Class");
			}

			public InnerStaticClass(String str) {
				this.str = str;
			}

			void print() {
				System.out.println("\nInnerStaticClass#print() : ");
				// System.out.println(instanceStr); //Not allowed
				System.out.println(staticeStr);
				System.out.println(str);
			}
		}

		class InnerInstanceClass {
			void print() {
				System.out.println("\nInnerInstanceClass#print() : ");
				System.out.println(instanceStr);
				System.out.println(staticeStr);
			}
		}

	}

	// Not Allowed
	/*
	 * public class AnotherClass{ }
	 */

	final class SubFinalClass extends OuterClass {

		public SubFinalClass(String instanceStr) {
			super(instanceStr);
		}

	}

	// Not Allowed
	/*
	 * class SubSubClass extends SubFinalClass{ }
	 */

	public static void main(String[] args) {
		OuterClass outerClassInstance = new OuterClass("Outer Class String1");
		new OuterClass.InnerStaticClass("OuterClass.InnerStaticClass String1").print();
		new OuterClass.InnerStaticClass().print();

		// new InnerInstanceClass(); //Error
		outerClassInstance.new InnerInstanceClass().print();
		new OuterClass("Outer Class String2").new InnerInstanceClass().print();

		// Ananymous Local Class
		new OuterClass("Outer Ananymous Class1") {
			@Override
			void print() {
				System.out.println("\n\nOverridden Print Method...");
				super.print();
			}
		}.print();
		
		final int val = 10000;

		// Another Ananymous Local Class
		OuterClass anotherAnanymousClassInstance = new OuterClass("Outer Ananymous Class2") {
			@Override
			void print() {
				System.out.println("\n\nAnother Overridden Print Method... Value: " + val);
				super.print();
			}
			
			void localPrint() {
				System.out.println("\nInside Local Print Method...");
				print();
			}
		};
		anotherAnanymousClassInstance.print();
		//anotherAnanymousClassInstance.localPrint(); // Won't work
		
		//Local Class
		class LocalClass extends OuterClass {
			public LocalClass(String instanceStr) {
				super(instanceStr);
				// TODO Auto-generated constructor stub
			}

			void localPrint() {
				System.out.println("\nInside Local Print Method...Value: " + val);
				print();
			}
		}
		
		LocalClass localClass = new LocalClass("Local Class");
		localClass.localPrint();
	}
}