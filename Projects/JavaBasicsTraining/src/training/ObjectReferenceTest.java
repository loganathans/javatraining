package training;

import java.util.Arrays;
import java.util.HashSet;

public class ObjectReferenceTest {

	public static void main(String[] args) {
		System.out.println("\n\n########### Mutability TEST 1 ##########");
		Employee emp = new Employee("Jack", "D001");
		emp.setDesignation("Tester");
		HashSet<String> skillSet = new HashSet<>(Arrays.asList(new String[]{"Manual", "Automation"}));
		System.out.println("Skill Sets earlier:" + skillSet);
		emp.setSkillSet(skillSet);
		System.out.println("\nBefore: >>" + emp);
		printEmployeeSkillset(emp);
		System.out.println("\nAfter: >>" + emp);
		System.out.println("Skill Sets later:" + skillSet);
		
		System.out.println("\n\n###########  Mutability TEST 2 ##########");
		skillSet = new HashSet<>(Arrays.asList(new String[]{"Java", ".Net"}));
		System.out.println("Skill Sets earlier:" + skillSet);
		emp = new Employee("Matt", "D002");
		emp.setDesignation("Developer");
		emp.setSkillSet(skillSet);
		System.out.println("\nBefore: >>" + emp);
		//Clearing skillset collection
		skillSet.clear();
		System.out.println("\nAfter: >>" + emp);
		System.out.println("Skill Sets later:" + skillSet);
		
		System.out.println("\n\n########### TEST Using Clone ##########");
		skillSet = new HashSet<>(Arrays.asList(new String[]{"Java", ".Net"}));
		System.out.println("Skill Sets earlier:" + skillSet);
		emp = new Employee("Matt", "D002");
		emp.setDesignation("Developer");
		emp.setSkillSet(skillSet);
		System.out.println("\nBefore: >>" + emp);
		try {
			printEmployeeSkillset(emp.clone());
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("\nAfter:>>" + emp);
		System.out.println("Skill Sets later:" + skillSet);
		
		System.out.println("\n\n########### Immutable TEST ##########");
		skillSet = new HashSet<>(Arrays.asList(new String[]{"ASP", "Spring"}));
		ImmutableEmployee immutableEmp = new ImmutableEmployee("Jack", "D001", "Developer", skillSet);
		System.out.println("\nBefore: >>" + immutableEmp);
		printImmutableEmployeeSkillset(immutableEmp);
		System.out.println("\nAfter: >>" + immutableEmp);
		
	}

	static void printEmployeeSkillset(Employee emp) {
		System.out.println(">>" + "Printing Employee Skillset: " + emp.getSkillSet());
		emp.setDesignation("Developer");
		emp.getSkillSet().clear();
	}
	
	static void printImmutableEmployeeSkillset(ImmutableEmployee emp) {
		System.out.println(">>" + "Printing Employee Skillset: " + Arrays.asList(emp.getSkillSet()));
		// emp.setDesignation("Developer"); // Error
		// emp.getSkillSet().clear(); // Not possible
	}


}
