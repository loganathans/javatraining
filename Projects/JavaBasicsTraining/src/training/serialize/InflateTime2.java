package training.serialize;

import java.io.ObjectInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Calendar;

public class InflateTime2 {
	public static void main(String[] args) {
		String filename = "time2.ser";
		if (args.length > 0) {
			filename = args[0];
		}
		PersistentTime2 time = null;
		FileInputStream fis = null;
		ObjectInputStream in = null;
		try {
			fis = new FileInputStream(filename);
			in = new ObjectInputStream(fis);
			time = (PersistentTime2) in.readObject();
			in.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
		}
		// print out restored time
		System.out.println("Flattened time: " + time.getTime());
		System.out.println("Flattened static time: " + PersistentTime2.getStaticTime());
		System.out.println();
		// print out the current time
		System.out.println("Current time: " + Calendar.getInstance().getTime());
	}
}