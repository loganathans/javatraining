package training.serialize;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;

class Student implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3446470342391954895L;

	private String name;
	private Date DOB;
	private int id;

	Student(String name, int idno, Date dob) {
		this.name = name;
		id = idno;
		DOB = dob;
	}

	public String toString() {
		return name + "\t" + id + "\t" + DOB + "\t";
	}
	

    private void writeObject(java.io.ObjectOutputStream stream)
            throws IOException {
        stream.writeObject(name);
        stream.writeInt(id);
        stream.writeObject(DOB);
    }

    private void readObject(java.io.ObjectInputStream stream)
            throws IOException, ClassNotFoundException {
        name = (String) stream.readObject();
        id = stream.readInt();
        DOB = (Date) stream.readObject();
    }

}