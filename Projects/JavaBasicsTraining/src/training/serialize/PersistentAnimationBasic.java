package training.serialize;

import java.io.Serializable;

public class PersistentAnimationBasic implements Serializable, Runnable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5732060345610942869L;
	transient private Thread animator;
	private int animationSpeed;
	private static final long timeLong = System.currentTimeMillis();

	public PersistentAnimationBasic(int animationSpeed) {
		this.animationSpeed = animationSpeed;
		animator = new Thread(this);
		animator.start();
	}

	public void run() {
		System.out.println("Animation stated.. ");
		int i = 0;
		while (i < 5) {
			i++;
			// do animation here
			System.out.println("Animating...");
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("Animation ended. ");
	}
}