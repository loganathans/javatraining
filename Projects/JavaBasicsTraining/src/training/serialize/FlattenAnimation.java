package training.serialize;

import java.io.ObjectOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class FlattenAnimation {
	public static void main(String[] args) {
		String filename = "animation.ser";
		if (args.length > 0) {
			filename = args[0];
		}
		PersistentAnimation time = new PersistentAnimation(10);
		FileOutputStream fos = null;
		ObjectOutputStream out = null;
		try {
			fos = new FileOutputStream(filename);
			out = new ObjectOutputStream(fos);
			out.writeObject(time);
			out.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
}