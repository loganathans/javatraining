package training.serialize;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class PersistentAnimation implements Serializable, Runnable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4033880696525097175L;
	transient private Thread animator;
	private int animationSpeed;

	public PersistentAnimation(int animationSpeed) {
		this.animationSpeed = animationSpeed;
		startAnimation();
	}

	public void run() {
		System.out.println("Animation stated.. ");
		int i = 0;
		while (i < 5) {
			i++;
			// do animation here
			System.out.println("Animating...");
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("Animation ended. ");
	}

	private void writeObject(ObjectOutputStream out) throws IOException {
		out.defaultWriteObject();
	}

	private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
		// our "pseudo-constructor"
		in.defaultReadObject();
		// now we are a "live" object again, so let's run rebuild and start
		startAnimation();

	}

	private void startAnimation() {
		animator = new Thread(this);
		animator.start();
	}
}