package training.serialize;

import java.io.ObjectInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Calendar;

public class InflateStudent2 {
	public static void main(String[] args) {
		String filename = "student.ser";
		if (args.length > 0) {
			filename = args[0];
		}
		Student student = null;
		FileInputStream fis = null;
		ObjectInputStream in = null;
		try {
			fis = new FileInputStream(filename);
			in = new ObjectInputStream(fis);
			student = (Student) in.readObject();
			in.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
		}
		// print out restored time
		System.out.println("Flattened Student: " + student);
	}
}