package training.serialize;

import java.io.ObjectInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Calendar;

public class InflateStudentExt {
	public static void main(String[] args) {
		String filename = "student-ext.ser";
		if (args.length > 0) {
			filename = args[0];
		}
		StudentExt student = null;
		FileInputStream fis = null;
		ObjectInputStream in = null;
		try {
			fis = new FileInputStream(filename);
			in = new ObjectInputStream(fis);
			student = (StudentExt) in.readObject();
			in.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
		}
		// print out restored time
		System.out.println("Flattened Student: " + student);
	}
}