package training.serialize;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class Singleton implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2969399254209649143L;
	private static Singleton instance = new Singleton();
	private int i;

	public static Singleton getInstance() {
		return instance;
	}

	private Singleton() {
	}

	private Object readResolve() {
		return instance;
	}
	
	public static void main(String[] args) throws Throwable {

		Singleton s = Singleton.getInstance();
		s.i = 5;

		ByteArrayOutputStream baos = new java.io.ByteArrayOutputStream();
		ObjectOutputStream oos = new java.io.ObjectOutputStream(baos);
		oos.writeObject(s);
		oos.close();
		System.out.println("Before Serialization: " + s);

		s.i = 7; // modified after serialization

		InputStream is = new ByteArrayInputStream(baos.toByteArray());
		ObjectInputStream ois = new ObjectInputStream(is);
		Singleton deserialized = (Singleton) ois.readObject();
		System.out.println(deserialized.i); // prints 7
		System.out.println("After Serialization: " + deserialized); // Same object
		System.out.println("Equality: " + (s == deserialized));
	}
}