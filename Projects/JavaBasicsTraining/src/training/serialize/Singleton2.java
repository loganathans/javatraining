package training.serialize;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class Singleton2 implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2969399254209649143L;
	private static Singleton2 instance = new Singleton2();
	private int i;

	public static Singleton2 getInstance() {
		return instance;
	}

	private Singleton2() {
	}
	
	private void readObject(ObjectInputStream ois) throws IOException, ClassNotFoundException {
		ois.defaultReadObject();
		instance = this; // New object assigned - not recommended.
	}

	private Object readResolve() {
		return instance; // Returning the new object
	}

	public static void main(String[] args) throws Throwable {

		Singleton2 s = Singleton2.getInstance();
		s.i = 5;

		ByteArrayOutputStream baos = new java.io.ByteArrayOutputStream();
		ObjectOutputStream oos = new java.io.ObjectOutputStream(baos);
		oos.writeObject(s);
		oos.close();
		System.out.println("Before Serialization: " + s);

		s.i = 7; // modified after serialization

		InputStream is = new ByteArrayInputStream(baos.toByteArray());
		ObjectInputStream ois = new ObjectInputStream(is);
		Singleton2 deserialized = (Singleton2) ois.readObject();
		System.out.println(deserialized.i); // prints 5
		System.out.println("After Serialization: " + deserialized); // Different Object
		System.out.println("Equality: " + (s == deserialized));
	}
}