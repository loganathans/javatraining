package training.serialize;

import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Properties;

class Student2 implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3446470342391954895L;

	private String name;
	private Date DOB;
	private int id;

	Student2(String name, int idno, Date dob) {
		this.name = name;
		id = idno;
		DOB = dob;
	}

	public String toString() {
		return name + "\t" + id + "\t" + DOB + "\t";
	}
	

    private void writeObject(java.io.ObjectOutputStream stream)
            throws IOException {
    	Properties properties = new Properties();
		properties.setProperty("name", name);
		properties.setProperty("id", "" + id);
		properties.setProperty("DOB", DateFormat.getDateInstance().format(DOB));
		stream.writeObject(properties);
    }

    private void readObject(java.io.ObjectInputStream stream)
            throws IOException, ClassNotFoundException {
    	Properties properties = (Properties) stream.readObject();
		name = properties.getProperty("name");
		id = Integer.parseInt(properties.getProperty("id"));
		try {
			DOB = DateFormat.getDateInstance().parse(properties.getProperty("DOB"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

}