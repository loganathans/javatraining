package training.serialize;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

public class PersistentTime implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 92152685947239526L;
	private Date time;
	
	private static final Date staticTime =  Calendar.getInstance().getTime();

	public PersistentTime() {
		time = staticTime;
	}

	public Date getTime() {
		return time;
	}

	public static Date getStaticTime() {
		return staticTime;
	}
}