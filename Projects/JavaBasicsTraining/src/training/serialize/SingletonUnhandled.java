package training.serialize;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class SingletonUnhandled implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2969399254209649143L;
	private static SingletonUnhandled instance = new SingletonUnhandled();
	private int i;

	public static SingletonUnhandled getInstance() {
		return instance;
	}

	private SingletonUnhandled() {
	}

	public static void main(String[] args) throws Throwable {

		SingletonUnhandled s = SingletonUnhandled.getInstance();
		s.i = 5;

		ByteArrayOutputStream baos = new java.io.ByteArrayOutputStream();
		ObjectOutputStream oos = new java.io.ObjectOutputStream(baos);
		oos.writeObject(s);
		oos.close();
		System.out.println("Before Serialization: " + s);

		s.i = 7; // modified after serialization

		InputStream is = new ByteArrayInputStream(baos.toByteArray());
		ObjectInputStream ois = new ObjectInputStream(is);
		SingletonUnhandled deserialized = (SingletonUnhandled) ois.readObject();
		System.out.println(deserialized.i); // prints 5
		System.out.println("After Serialization: " + deserialized); // New instance created
		System.out.println("Equality: " + (s == deserialized));
	}
}