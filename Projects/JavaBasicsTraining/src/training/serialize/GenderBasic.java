package training.serialize;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class GenderBasic implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1795801413820233255L;
	public final static GenderBasic MALE = new GenderBasic("Male");
	public final static GenderBasic FEMALE = new GenderBasic("Female");

	private String name;

	private GenderBasic(String name) {
		this.name = name;
	}
	
	public static void main(String[] args) throws Throwable {
		GenderBasic s = MALE;

		ByteArrayOutputStream baos = new java.io.ByteArrayOutputStream();
		ObjectOutputStream oos = new java.io.ObjectOutputStream(baos);
		oos.writeObject(s);
		oos.close();
		System.out.println("Before Serialization: " + s);

		InputStream is = new ByteArrayInputStream(baos.toByteArray());
		ObjectInputStream ois = new ObjectInputStream(is);
		GenderBasic deserialized = (GenderBasic) ois.readObject();
		System.out.println("After Serialization: " + deserialized); // New instance created
		System.out.println("Equality: " + (s == deserialized));
	}
}