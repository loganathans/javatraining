package training.serialize;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.Calendar;

public class PersistentTime2 implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 92152685947239526L;
	private Date time;

	private static Date staticTime = Calendar.getInstance().getTime();

	public PersistentTime2() {
		time = staticTime;
	}

	public Date getTime() {
		return time;
	}

	public static Date getStaticTime() {
		return staticTime;
	}

	private void writeObject(ObjectOutputStream out) throws IOException {
		out.defaultWriteObject();
	}

	private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
		// our "pseudo-constructor"
		in.defaultReadObject();
	}
}