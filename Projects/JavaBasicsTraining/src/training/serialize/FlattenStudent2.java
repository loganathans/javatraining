package training.serialize;

import java.io.ObjectOutputStream;
import java.util.Date;
import java.io.FileOutputStream;
import java.io.IOException;

public class FlattenStudent2 {
	public static void main(String[] args) {
		String filename = "student2.ser";
		if (args.length > 0) {
			filename = args[0];
		}
		Student2 time = new Student2("ABC", 1, new Date());
		FileOutputStream fos = null;
		ObjectOutputStream out = null;
		try {
			fos = new FileOutputStream(filename);
			out = new ObjectOutputStream(fos);
			out.writeObject(time);
			out.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
}