package training.serialize;

import java.io.ObjectOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class FlattenAnimationBasic {
	public static void main(String[] args) {
		String filename = "animation-basic.ser";
		if (args.length > 0) {
			filename = args[0];
		}
		PersistentAnimationBasic time = new PersistentAnimationBasic(10);
		FileOutputStream fos = null;
		ObjectOutputStream out = null;
		try {
			fos = new FileOutputStream(filename);
			out = new ObjectOutputStream(fos);
			out.writeObject(time);
			out.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
}