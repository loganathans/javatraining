package training.serialize;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Properties;

class StudentExt implements java.io.Externalizable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 3446470342391954895L;
	
	private String name;
	private Date DOB;
	private int id;
	
	public StudentExt() {
		//Required public default constructor for externalizing (readExternal)
	}

	StudentExt(String name, int idno, Date dob) {
        this.name = name;
        id = idno;
        DOB = dob;
    }

	@Override
	public void readExternal(ObjectInput stream) throws IOException, ClassNotFoundException {
		Properties properties = (Properties) stream.readObject();
		name = properties.getProperty("name");
		id = Integer.parseInt(properties.getProperty("id"));
		try {
			DOB = DateFormat.getDateInstance().parse(properties.getProperty("DOB"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void writeExternal(ObjectOutput stream) throws IOException {
		Properties properties = new Properties();
		properties.setProperty("name", name);
		properties.setProperty("id", "" + id);
		properties.setProperty("DOB", DateFormat.getDateInstance().format(DOB));
		stream.writeObject(properties);
	}


    public String toString() {
        return name + "\t" + id + "\t" + DOB + "\t";
    }

}