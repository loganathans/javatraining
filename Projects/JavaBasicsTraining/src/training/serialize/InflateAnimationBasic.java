package training.serialize;

import java.io.ObjectInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Calendar;

public class InflateAnimationBasic {
	public static void main(String[] args) {
		String filename = "animation-basic.ser";
		if (args.length > 0) {
			filename = args[0];
		}
		FileInputStream fis = null;
		ObjectInputStream in = null;
		try {
			fis = new FileInputStream(filename);
			in = new ObjectInputStream(fis);
			PersistentAnimationBasic animation = (PersistentAnimationBasic) in.readObject();
			in.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
		}
	}
}