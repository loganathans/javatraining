package training.serialize;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamException;
import java.io.Serializable;

/**
 * Assume it to be some huge object
 *
 */
public class Gender implements Serializable {
	/**
	 * The only two instances
	 */
	public final static Gender MALE = new Gender("Male");
	public final static Gender FEMALE = new Gender("Female");

	private String name;

	private Gender(String name) {
		this.name = name;
	}

	Object writeReplace() throws ObjectStreamException {
		// Serialize in simplified form instead of storing the whole object,
		// which is unnecessary
		if (this.equals(MALE)) {
			return SerializedForm.MALE_FORM;
		} else {
			return SerializedForm.FEMALE_FORM;
		}
	}

	/**
	 * Simple serialized form to just hold the type in integer
	 *
	 */
	private static class SerializedForm implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = -291001142794326909L;
		final static SerializedForm MALE_FORM = new SerializedForm(0);
		final static SerializedForm FEMALE_FORM = new SerializedForm(1);

		private int value;

		SerializedForm(int value) {
			this.value = value;
		}

		Object readResolve() throws ObjectStreamException {
			// Return from one of the two instances
			if (value == MALE_FORM.value) {
				return Gender.MALE;
			} else {
				return Gender.FEMALE;
			}
		}
	}

	public static void main(String[] args) throws Throwable {
		Gender s = MALE;

		ByteArrayOutputStream baos = new java.io.ByteArrayOutputStream();
		ObjectOutputStream oos = new java.io.ObjectOutputStream(baos);
		oos.writeObject(s);
		oos.close();
		System.out.println("Before Serialization: " + s);

		InputStream is = new ByteArrayInputStream(baos.toByteArray());
		ObjectInputStream ois = new ObjectInputStream(is);
		Gender deserialized = (Gender) ois.readObject();
		System.out.println("After Serialization: " + deserialized); // New
																	// instance
																	// created
		System.out.println("Equality: " + (s == deserialized));
	}
}