package training.inheritance;

public abstract class CarnivorousAnimal implements Animal {
	@Override
	public EatingHabit getEatingType() {
		return EatingTypes.CARNIVORUOUS;
	}
}
