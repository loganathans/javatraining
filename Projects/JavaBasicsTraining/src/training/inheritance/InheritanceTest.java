package training.inheritance;

public class InheritanceTest {

	public static void main(String[] args) {
		System.out.println("\nCow: ");
		Animal cow1 = new Cow();
		Animal.printAnimalActions(cow1);

		System.out.println("\nCuckoo: ");
		Animal cuckoo1 = new Cuckoo();
		Animal.printAnimalActions(cuckoo1);

		System.out.println("\nDog: ");
		Animal dog1 = new Dog();
		Animal.printAnimalActions(dog1);

		System.out.println("\nTypes of Eating Habits..");
		EatingTypes[] values = EatingTypes.values();
		for (EatingTypes eatingType : values) {
			System.out.println((eatingType.ordinal() + 1) + ") " + eatingType.getName());
		}

	}

}
