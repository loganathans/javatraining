package training.inheritance;

public abstract class OmnivorousAnimal implements Animal{

	@Override
	public EatingHabit getEatingType() {
		return EatingTypes.OMNIVOROUS;
	}

}
