package training.inheritance;

public interface EatingHabit {
	
	String getName();
	
	void eat();
	
	static void printEatingType(EatingHabit eatingHabit) {
		eatingHabit.eat();
	}
	
}
