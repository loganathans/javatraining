package training.inheritance;

public class Dog extends OmnivorousAnimal{

	@Override
	public void makeNoice() {
		System.out.println("Bow..! Bow..!!");
	}

	@Override
	public DomesticationType getDomesticationType() {
		return DomesticationType.DOMESTIC;
	}

}
