package training.inheritance;

public interface Animal {
	public void makeNoice();

	public EatingHabit getEatingType();

	public default void eat() {
		getEatingType().eat();
	}
	
	DomesticationType getDomesticationType();

	static void printAnimalActions(Animal animal) {
		animal.makeNoice();
		animal.eat();
		System.out.println("DomesticationType type: " + animal.getDomesticationType().name().toLowerCase() + " animal");
		if (animal instanceof Bird) {
			Bird bird = (Bird) animal;
			bird.fly();
		}
	}
}
