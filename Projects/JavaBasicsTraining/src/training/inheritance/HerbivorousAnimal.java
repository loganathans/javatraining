package training.inheritance;

public abstract class HerbivorousAnimal implements Animal {
	@Override
	public EatingHabit getEatingType() {
		return EatingTypes.HERBIVOROUS;
	}
}
