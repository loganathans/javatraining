package training.inheritance;

public class Cow extends HerbivorousAnimal {

	@Override
	public void makeNoice() {
		System.out.println("Mow!!");
	}

	@Override
	public DomesticationType getDomesticationType() {
		return DomesticationType.DOMESTIC;
	}

}
