package training.inheritance;

public class Cuckoo extends CarnivorousAnimal implements Bird {

	@Override
	public void makeNoice() {
		System.out.println("koo.. koo..");
	}

	@Override
	public void fly() {
		System.out.println("Flying...");
	}
	
	@Override
	public DomesticationType getDomesticationType() {
		return DomesticationType.WILD;
	}

}
