package training.inheritance;

public class OverriddeDefaultMethodTest {

	public static interface BaseInterface1 {

		default void print() {
			System.out.println("BaseInterface1.print()");
		}

	}

	public static interface BaseInterface2 {

		default void print() {
			System.out.println("BaseInterface2.print()");
		}

	}

	public static interface SubInterface extends BaseInterface1, BaseInterface2 {

		// Must override the default method within this interface, Otherwise
		// compilation error.
		@Override
		default void print() {
			System.out.println("Overriding in SubInterface");
			BaseInterface1.super.print();
			BaseInterface2.super.print();
		}

	}
	
	public static class SubClass1 implements SubInterface {
	}

	public static class SubClass2 implements SubInterface {
		// Must override the default method within this interface, Otherwise
		// compilation error.
		@Override
		public void print() {
			System.out.println("Overriding in SubClass2");
			//super.print(); // Not allowed. Here super only refers to a super class namely Object
			//BaseInterface1.super.print();  // Not allowed. cannot bypass the call to the ancestor of the parent interface.
			SubInterface.super.print();
		}
	}

	public static class SubClass3 implements BaseInterface1, BaseInterface2 {
		@Override
		public void print() {
			System.out.println("Overriding in SubClass2");
			BaseInterface1.super.print();
			BaseInterface2.super.print();
		}
	}
	
	public static void main(String[] args) {
		System.out.println("Sublcass1....");
		new SubClass1().print();
		System.out.println("Sublcass2....");
		new SubClass2().print();
		System.out.println("Sublcass3....");
		new SubClass3().print();
	}

}
