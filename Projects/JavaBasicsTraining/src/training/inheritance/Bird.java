package training.inheritance;

public interface Bird extends Animal {
	public void fly();
}
