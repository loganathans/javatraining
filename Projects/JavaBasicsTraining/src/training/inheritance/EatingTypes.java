package training.inheritance;

public enum EatingTypes implements EatingHabit{
	
	CARNIVORUOUS ("Non-Vegtarian") {
		@Override
		public void eat() {
			System.out.println("Eating " + getName() + " foods..");			
		}
		
	}, HERBIVOROUS("Vegtarian"){
		@Override
		public void eat() {
			System.out.println("Eating " + getName() + " foods..");			
		}
	}, OMNIVOROUS("Both Veg and Non-veg"){
		@Override
		public void eat() {
			CARNIVORUOUS.eat();
			System.out.println("And");
			HERBIVOROUS.eat();
		}
	};
	
	private final String name;

	private EatingTypes(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}

}
