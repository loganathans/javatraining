package training.accessmod.test;

import training.accessmod.PublicClass;

public class AccessModTest {

	public static void main(String[] args) {
		PublicClass publicClass = new PublicClass();
//		new PackageLevelClass(); //Error
		
		System.out.println(publicClass.getPublicStr());
//		System.out.println(publicClass.getProtectedStr()); // Error
//		System.out.println(publicClass.getPackageLevelStr()); // Error
//		System.out.println(publicClass.getPrivateStr()); // Error
		
		System.out.println(new SubPublicClass().getProtectedStr());
		
	}

}
