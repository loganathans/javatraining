package training.accessmod.test;

import training.accessmod.PublicClass;

public class SubPublicClass extends PublicClass {
	@Override
	protected String getProtectedStr() {
		return super.getProtectedStr() + " invoked by sub-class..";
		
	}
}
