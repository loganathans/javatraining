package training.accessmod;

public class AccessModTest {

	public static void main(String[] args) {
		PublicClass publicClass = new PublicClass();
		PackageLevelClass packageLevelClass = new PackageLevelClass(); //No Error
		
		System.out.println(publicClass.publicStr);
		System.out.println(publicClass.protectedStr);
		System.out.println(publicClass.packageLevelStr);
		//System.out.println(publicClass.privateStr); // Error
	}

}
