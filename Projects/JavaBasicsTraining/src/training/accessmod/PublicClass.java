package training.accessmod;

public class PublicClass {
	public String publicStr = "Public"; // Not recommended
	protected String protectedStr = "Protected";
	String packageLevelStr = "PackageLevel";
	private String privateStr = "Private";

	public String getPublicStr() {
		return publicStr;
	}

	public void setPublicStr(String publicStr) {
		this.publicStr = publicStr;
	}

	protected String getProtectedStr() {
		return protectedStr;
	}

	protected void setProtectedStr(String protectedStr) {
		this.protectedStr = protectedStr;
	}

	String getPackageLevelStr() {
		return packageLevelStr;
	}

	void setPackageLevelStr(String packageLevelStr) {
		this.packageLevelStr = packageLevelStr;
	}

	private String getPrivateStr() {
		return privateStr;
	}

	private void setPrivateStr(String privateStr) {
		this.privateStr = privateStr;
	}

	PublicClass createCopy() {
		PublicClass publicClassCopy = new PublicClass();
		// Access to private member of another instance of the same class is
		// available within the class
		// publicClassCopy.privateStr = this.privateStr;
		publicClassCopy.setPrivateStr(getPrivateStr());
		return publicClassCopy;
	}

}
