package training;

import java.io.IOException;

public class Overriding {
	class EBase extends Exception {
	}

	class ESub extends EBase {
	}

	class RBase {
	}

	class RSub extends RBase {
	}

	abstract class A {
		abstract RBase a() throws EBase;
	}

	abstract class B extends A {
		abstract RSub a() throws ESub;
	}
	
	abstract class C extends B {
		abstract RSub a();
	}
	
	abstract class C2 extends B {
		//abstract RSub a() throws EBase;// not allowed
	}
	
	abstract class C3 extends B {
		//abstract RSub a() throws ESub, IOException;// not allowed
	}
}