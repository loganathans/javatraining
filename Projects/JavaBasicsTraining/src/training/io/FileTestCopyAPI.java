package training.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class FileTestCopyAPI {
	private final static File target = new File("D:\\temp\\java1");


	public static void main(String[] args) {
		File directory = new File("D:\\temp\\java");
		if(directory.exists() && directory.isDirectory()) {
			File[] children = directory.listFiles();
			for (File child : children) {
				if(child.isFile()) {
					try {
						copyFile(child);
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (NullPointerException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} finally {
						
					}
				}
			}
		}

	}

	private static void copyFile(File inputFile) throws IOException {
		try (FileInputStream fileInputStream  = new FileInputStream(inputFile);){
			Files.copy(fileInputStream, Paths.get(target.getAbsolutePath(), inputFile.getName()), StandardCopyOption.REPLACE_EXISTING);
		}
	}

}
