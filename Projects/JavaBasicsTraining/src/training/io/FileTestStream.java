package training.io;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileTestStream {
	private final static File target = new File("D:\\temp\\java1");


	public static void main(String[] args) {
		File directory = new File("D:\\temp\\java");
		if(directory.exists() && directory.isDirectory()) {
			File[] children = directory.listFiles();
			for (File child : children) {
				if(child.isFile()) {
					try {
						copyFile(child);
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (NullPointerException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} finally {
						
					}
				}
			}
		}

	}

	private static void copyFile(File inputFile) throws IOException {
		FileInputStream fileInputStream = null;
		FileOutputStream fileOutputStream = null;
		try {
			fileInputStream = new FileInputStream(inputFile);
			BufferedInputStream bis = new BufferedInputStream(fileInputStream);
			byte[] buff =new byte[1024];
			fileOutputStream = new FileOutputStream(new File(target, inputFile.getName()));
			BufferedOutputStream bos = new BufferedOutputStream(fileOutputStream);
			while(bis.read(buff) != -1) {
				bos.write(buff);
				bos.flush();
			}
		} finally {
			try {
				if(fileInputStream != null) {
					fileInputStream.close();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			try {
				if(fileOutputStream != null) {
					fileOutputStream.close();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
