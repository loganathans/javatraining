package training.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class FileTestLinesStreamJava8 {
	private final static File target = new File("D:\\temp\\java1");


	public static void main(String[] args) {
		File directory = new File("D:\\temp\\java");
		if(directory.exists() && directory.isDirectory()) {
			File[] children = directory.listFiles();
			for (File child : children) {
				if(child.isFile()) {
					try {
						printFileContent(child);
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (NullPointerException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} finally {
						
					}
				}
			}
		}

	}

	private static void printFileContent(File inputFile) throws IOException {
			Stream<String> lines = Files.lines(Paths.get(inputFile.getPath()));
			lines.forEach(line1 -> System.out.println(line1));
	}

}
