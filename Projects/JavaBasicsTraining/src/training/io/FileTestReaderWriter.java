package training.io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileTestReaderWriter {
	private final static File target = new File("D:\\temp\\java1");


	public static void main(String[] args) {
		File directory = new File("D:\\temp\\java");
		if(directory.exists() && directory.isDirectory()) {
			File[] children = directory.listFiles();
			for (File child : children) {
				if(child.isFile()) {
					try {
						copyFile(child);
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (NullPointerException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} finally {
						
					}
				}
			}
		}

	}

	private static void copyFile(File inputFile) throws IOException {
		FileReader fileInputReader = null;
		FileWriter fileOutputWriter = null;
		try {
			fileInputReader = new FileReader(inputFile);
			BufferedReader bis = new BufferedReader(fileInputReader);
			byte[] buff =new byte[1024];
			fileOutputWriter = new FileWriter(new File(target, inputFile.getName()));
			BufferedWriter bos = new BufferedWriter(fileOutputWriter);
			String line = null;
			while((line = bis.readLine()) != null) {
				bos.write(line);
				bos.write(System.getProperty("line.separator"));
				bos.flush();
			}
		} finally {
			try {
				if(fileInputReader != null) {
					fileInputReader.close();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			try {
				if(fileOutputWriter != null) {
					fileOutputWriter.close();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
