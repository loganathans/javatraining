package training;
//Alert! don't do hiding..
public class Hiding {
	
	static class BaseClass {
		protected int val = 10;
		
		public static void printSquare(int val) {
			System.out.println("Square of " + val + " is:" + (val * val));
		}
	}
	
	static class SubClass extends BaseClass {
		protected int val = 20;
	}
	
	static class SubClass2 extends BaseClass {
		
		public static void printSquare(int val) {
			System.out.println("Below is a square:");
			System.out.println(" _____");
			System.out.println("|     |");
			System.out.println("|     |");
			System.out.println(" -----");
		}
	}
	
	public static void main(String[] args) {
		BaseClass instance = new BaseClass();
		BaseClass.printSquare(instance.val);
		
		SubClass subclassInstance = new SubClass();
		SubClass.printSquare(subclassInstance.val); // Can't get the superclass val
		BaseClass subclassInstanceReference = subclassInstance;
		SubClass.printSquare(subclassInstanceReference.val); // Unexpected behaviour programming mistakes.
		
		SubClass2 subclassInstance2 = new SubClass2();
		SubClass2.printSquare(subclassInstance2.val); // Unexpected behaviour  programming mistakes.
		
	}

}
