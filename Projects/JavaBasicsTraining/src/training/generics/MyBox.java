package training.generics;

import training.Employee;

public class MyBox<T extends Number> {
	private T t;

	public MyBox(T t) {
		this.t = t;

	}

	public <U> void print(U u) {
		System.out.println(u.toString() + " " + t.toString());
	}

	public void print() {
		System.out.println(this.toString());
	}

	@Override
	public String toString() {
		return getContent().toString();
	}

	public T getContent() {
		return t;
	}

	public void setContent(T t) {
		this.t = t;
	}

	public static void main(String[] args) {
		MyBox<Float> myBox = new MyBox<>(1.5f);
		myBox.print();
		// myBox.setContent(10); //Not allowed
		myBox.setContent(2.5f);
		myBox.print();

		MyBox<Integer> myBox2 = new MyBox<>(1000);
		myBox2.print();
		// myBox2.setContent(12.5f);// Not allowed
		myBox2.setContent(20);
		myBox2.print();
		
		//MyBox<String> myBox3 = new MyBox<>("Hi"); // Not allowed

		// Template Method
		myBox2.print("Hello");
		myBox2.print(new Employee("Steve", "112"));

		// Wildcard
		MyBox<?> myUnknownNumberBox = new MyBox<Float>(1.5f);
		myUnknownNumberBox.print();
		myUnknownNumberBox = new MyBox<Integer>(Integer.valueOf(10));
		myUnknownNumberBox.print();

		// Lower bound wildcard
		MyBox<? extends Number> myNumberBox = new MyBox<Float>(1.5f);
		myNumberBox.print();
		myNumberBox = new MyBox<Integer>(Integer.valueOf(10));
		myNumberBox.print();

		// Upper bound wildcard
		MyBox<? super Integer> myNumberBox2 = new MyBox<>(Integer.valueOf(10));
		myNumberBox2.print();
		// myNumberBox2 = new MyBox<Float>(1.5f); //Not allowed
		myNumberBox2 = new MyBox<Number>(30); 
		myNumberBox2.print();

	}

}
