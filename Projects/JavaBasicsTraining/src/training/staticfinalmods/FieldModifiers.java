package training.staticfinalmods;

public class FieldModifiers {
	
	static class Class {
		private String instanceStr = "Instance String";
		private final String  instanceFinalStr = "Final Instance String";
		private final String  anotherInstanceFinalStr;
		private final String  thirdInstanceFinalStr;
		
		//Instance Initializer
		{
			String initialValue = getInitialValue();
			thirdInstanceFinalStr = initialValue;
		}

		private String getInitialValue() {
			return "thirdInstanceFinalStr - from Initializer";
		}
		
		private static String staticStr = "Static String";
		private static final String CONSTANT = "I am Constant";
		private static final String ANOTHER_CONSTANT;
		
		// Static Initializer
		static{
			String anotherConstantValue = getAnotherConstantValue();
			ANOTHER_CONSTANT = anotherConstantValue;
		}

		private static String getAnotherConstantValue() {
			return "I am another constant";
		}
		
		public Class() {
			anotherInstanceFinalStr = "anotherInstanceFinalStr";
		}
	}

	public static void main(String[] args) {
		Class instance = new Class();
		
		instance.instanceStr = instance.instanceStr + " - modified"; 
		System.out.println(instance.instanceStr);
		
		//instance.instanceFinalStr = instance.instanceFinalStr + "1"; //Not Allowed 
		System.out.println(instance.instanceFinalStr);
		
		System.out.println(instance.anotherInstanceFinalStr);
		
		System.out.println(instance.thirdInstanceFinalStr);
		
		Class.staticStr = Class.staticStr + " - modified";
		System.out.println(Class.staticStr);
		
		//Class.CONSTANT = "Modified"; //Not Allowed
		System.out.println(Class.CONSTANT);
		
		//ANOTHER_CONSTANT = "Modified"; //Not allowed
		System.out.println(Class.ANOTHER_CONSTANT);
	}

}
