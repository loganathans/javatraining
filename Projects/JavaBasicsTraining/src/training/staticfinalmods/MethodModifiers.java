package training.staticfinalmods;

public class MethodModifiers {
	
	interface MyBaseInterface {
		void myBaseMethod();
	}
	
	interface AnotherBaseInterface {
		void anotherBaseMethod();
	}
	
	interface MySubInterface extends MyBaseInterface {
		void mySubInterfaceMethod();
	}
	
	abstract static class AbstractBaseClass implements MySubInterface, AnotherBaseInterface{
		public void publicMethod() {
			System.out.println("Base Public Method");
		}
		
		protected abstract void abstractPublicMethod();
		
		public final void finalPublicMehtod() {
			System.out.println("Final Public Method");
		}
		
		@Override
		public void anotherBaseMethod() {
			System.out.println("Another Base Method");			
		}
	}
	
	
	static class SubClass extends AbstractBaseClass {


		@Override
		public void myBaseMethod() {
			System.out.println("Base Method Implementation");			
		}
		
		@Override
		public void mySubInterfaceMethod() {
			System.out.println("SubInterface Method Implementation");
		}
		
		@Override
		protected void abstractPublicMethod() {
			// super.abstractPublicMethod(); // Not allowed
			System.out.println("Abstract Public Method Implementation");			
		}
		
		@Override
		public void publicMethod() {
			System.out.println("Subclass Public Method");
			super.publicMethod();
			System.out.println("\t - Overrridden in Subclass");
		}
		
		//Not Allowed
		/*@Override
		public void finalPublicMehtod() {
		}*/
		
	}
	
	public static void main(String[] args) {
		SubClass subClass = new SubClass();
		subClass.publicMethod();
		subClass.myBaseMethod();
		subClass.anotherBaseMethod();
		subClass.abstractPublicMethod();
		subClass.finalPublicMehtod();
		
		// new SingletonClass(); // Not allowed
		SingletonClass.INSTANCE.print();
		
		LazySingletonClass.getInstance().print();
	}
	
}


final class  SingletonClass {
	
	private SingletonClass(){
	}
	
	public static final SingletonClass INSTANCE = new SingletonClass();
	
	public void print() {
		System.out.println("I am a single instance..!");
	}
}

final class  LazySingletonClass {
	
	private LazySingletonClass(){
		initialize();
	}
	private void initialize() {
		System.out.println("Doing a resourceful initialization..");
	}
	
	private static LazySingletonClass instance;

	public static LazySingletonClass getInstance() {
		if(instance == null) {
			instance = new LazySingletonClass();
		}
		
		return instance;
	}
	public void print() {
		System.out.println("I am a lazy singleton instance..!");
	}
}
