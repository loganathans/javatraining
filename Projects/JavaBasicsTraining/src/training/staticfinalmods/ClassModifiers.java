package training.staticfinalmods;

public class ClassModifiers {
	public static class OuterClass {

		static class InnerStaticClass {
		}

		class InnerInstanceClass {
			
			//Not Allowed
			/*static class InnerInnerInnerClass{
			}*/
		}

	}

	// Not Allowed
	/*
	 * public class AnotherClass{ }
	 */

	final class SubFinalClass extends OuterClass {
	}

	// Not Allowed
	/*
	 * class SubSubClass extends SubFinalClass{ }
	 */

	public static void main(String[] args) {
		OuterClass outerClassInstance = new OuterClass();
		new OuterClass.InnerStaticClass();

		// new InnerInstanceClass(); //Error
		outerClassInstance.new InnerInstanceClass();
	}
}