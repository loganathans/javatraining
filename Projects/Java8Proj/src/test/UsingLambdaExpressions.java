package test;

import static test.Person.Gender.FEMALE;
import static test.Person.Gender.MALE;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class UsingLambdaExpressions {

    private static ArrayList<Person> roster;

    public static void main(String[] args) {
        setup();
        test();
    }

    private static void setup() {
        roster = new ArrayList<>();
        roster.add(new Person("Alice", FEMALE, 28));
        roster.add(new Person("Candy", FEMALE, 19));
        roster.add(new Person("Bob", MALE, 24));
        roster.add(new Person("Pandi", MALE, 36));
        roster.add(new Person("David", MALE, 21));
        roster.add(new Person("Mani", MALE, 30));
    }

    private static void test() {
        // printPersonsOlderThan(roster, 25);

        // printPersonsWithinAgeRange(roster, 25, 30);

        // // Print person eligible for service
        // printPersons(roster, new CheckPersonEligibleForSelectiveService());

        // printPersons(roster, new CheckPerson(){
        //
        // @Override
        // public boolean test(Person p) {
        // return p.getAge() <25;
        // }
        //
        // });

        // // Print person eligible for service using unanimous class
        // printPersonsBasedOnCrieteria();

        // // Print person by filtering with lambda expressions
        // printPersonByLambdaExpressions();

        // // Print person by filtering using standard Java Funtional Interfaces
        // usingStanderdFunctionaInterfaces();

        // // Scoping of variables in labda Expression
        // scopingOfVariable();

        // // Sort using compare
        // sortUsingLambdaExpressionInComparator();

        // // creating Thread Using LambdaExpression
        // creatingThreadUsingLambdaExpression();

         methodRefernces();

    }

    private static void methodRefernces() {
        Person[] rosterAsArray = roster.toArray(new Person[roster.size()]);

//        sortingUsingComparator(rosterAsArray);

//         sortingUsingComparatorAndLE(rosterAsArray);
//
//         sortingUsingCompareMethodInLE(rosterAsArray);
//
         sortingUsingCompareMethodReferenceInLE(rosterAsArray);

        System.out.println(Arrays.asList(rosterAsArray));
        
        //Method reference with first argument as target and rest as argument to the target method
		print(Target::sum);


    }

    private static void sortingUsingCompareMethodReferenceInLE(Person[] rosterAsArray) {
        // Static method reference
//        Arrays.sort(rosterAsArray, Person::compareByName_staticMethod);
//
//        // // Instance Method reference
//         ComparisonProvider comparisonProvider = new ComparisonProvider();
//         Arrays.sort(rosterAsArray, comparisonProvider::compareByName);

        // //Reference to an Instance Method of an Arbitrary Object of a Particular Type
         Arrays.sort(rosterAsArray, Person::compareByName_instanceMethod);
        compareByNameWihNewInterface(Person::compareByNameNew);

    }

    @FunctionalInterface
    public static interface ComparatorNew {
        int compareByName(Person p2, Person p3);
    }

    private static int compareByNameWihNewInterface(ComparatorNew c) {
        return 0;
    }


    private static void sortingUsingCompareMethodInLE(Person[] rosterAsArray) {
        Arrays.sort(rosterAsArray, (a, b) -> Person.compareByName_staticMethod(a, b));
    }

    private static void sortingUsingComparatorAndLE(Person[] rosterAsArray) {
        Arrays.sort(rosterAsArray, (Person a, Person b) -> {
            return a.getName().compareTo(b.getName());
        });
    }

    private static void sortingUsingComparator(Person[] rosterAsArray) {
        class PersonAgeComparator implements Comparator<Person> {
            @Override
            public int compare(Person a, Person b) {
                return a.getName().compareTo(b.getName());
            }
        }

        Arrays.sort(rosterAsArray, new PersonAgeComparator());
    }

    private static void creatingThreadUsingLambdaExpression() {
        new Thread(() -> System.out.println("Hello World")).start();
        new Thread(() -> System.out.println("Hai World")).start();
    }

    private static void scopingOfVariable() {
        final String greeting1 = "Hello";
        accept(roster.get(0), p -> System.out.println(greeting1 + " " + p.getName()));

        String greeting2 = "Hai"; // Effectively final
        // greeting2 = "Hello"; // Results in compiler error
        accept(roster.get(1), p -> System.out.println(greeting2 + " " + p.getName()));

    }

    private static void accept(Person p, Consumer<Person> consumer) {
        consumer.accept(p);
    }

    // Search for person older than age
    public static void printPersonsOlderThan(List<Person> roster, int age) {
        for (Person p : roster) {
            if (p.getAge() >= age) {
                p.printPerson();
            }
        }
    }

    // Search for person within age
    public static void printPersonsWithinAgeRange(List<Person> roster, int low, int high) {
        for (Person p : roster) {
            if (low <= p.getAge() && p.getAge() < high) {
                p.printPerson();
            }
        }
    }

    // Search person based on a criteria
    public static void printPersons(List<Person> roster, CheckPerson tester) {
        for (Person p : roster) {
            if (tester.test(p)) {
                p.printPerson();
            }
        }
    }

    private static void printPersonsBasedOnCrieteria() {
        printPersons(roster, new CheckPerson() {
            @Override
            public boolean test(Person p) {
                return p.getGender() == Person.Gender.MALE && p.getAge() >= 18 && p.getAge() <= 25;
            }
        });
    }

    private static void printPersonByLambdaExpressions() {
        // printPersons(roster, (Person p) -> p.getGender() == Person.Gender.MALE && p.getAge() >= 18 && p.getAge() <=
        // 25);

        // printPersons(roster, p -> p.getGender() == Person.Gender.FEMALE && p.getAge() >= 18 && p.getAge() <= 25);
        //
        // printPersons(roster, (Person p) -> p.getGender() == Person.Gender.MALE && p.getAge() > 25);
        //
        printPersons(roster, p -> {
            int age = p.getAge();
            return age <= 25;
        });
    }

    private static void usingStanderdFunctionaInterfaces() {
        // printUsingPredicate();

        // processUsingConsumer();

        // processUsingFunction();

    }

    private static void processUsingFunction() {
        processPersonsWithFunction(roster, p -> p.getGender() == Person.Gender.MALE && p.getAge() >= 18
                && p.getAge() <= 25, p -> p.getEmailAddress(), // Obtains email from person
                email -> System.out.println(email) // Prints the email
        );

    }

    private static void processUsingConsumer() {
        processPersons(roster, p -> p.getGender() == Person.Gender.MALE && p.getAge() >= 18 && p.getAge() <= 25,
                p -> p.printPerson());

    }

    private static void printUsingPredicate() {
        printPersonsWithPredicate(roster, p -> p.getGender() == Person.Gender.MALE && p.getAge() >= 18
                && p.getAge() <= 25);
        //
        // printPersonsWithPredicate(roster, (Person p) -> p.getGender() == Person.Gender.FEMALE && p.getAge() >= 18 &&
        // p.getAge() <=
        // 25);
        //
        // printPersonsWithPredicate(roster, (Person p) -> p.getGender() == Person.Gender.MALE && p.getAge() > 25);
        //
        // printPersonsWithPredicate(roster, (Person p) -> p.getAge() <= 25);
    }

    public static void printPersonsWithPredicate(List<Person> roster, Predicate<Person> tester) {
        for (Person p : roster) {
            if (tester.test(p)) {
                p.printPerson();
            }
        }
    }

    private static void sortUsingLambdaExpressionInComparator() {
        Person[] persons = roster.toArray(new Person[0]);
        Arrays.sort(persons, (p1, p2) -> p1.getName().compareTo(p2.getName()));
        System.out.println(Arrays.asList(persons));

        Arrays.sort(persons, (p1, p2) -> -1 * p1.getName().compareTo(p2.getName()));
        System.out.println(Arrays.asList(persons));
    }

    public static void processPersons(List<Person> roster, Predicate<Person> tester, Consumer<Person> block) {
        for (Person p : roster) {
            if (tester.test(p)) {
                block.accept(p);
            }
        }
    }

    public static void processPersonsWithFunction(List<Person> roster, Predicate<Person> tester,
            Function<Person, String> mapper, Consumer<String> block) {
        for (Person p : roster) {
            if (tester.test(p)) {
                String data = mapper.apply(p);
                block.accept(data);
            }
        }
    }

    static class ComparisonProvider {
        public int compareByName(Person a, Person b) {
            return a.getName().compareTo(b.getName());
        }
    }
    
    

	interface FunctionWithTarget {
		int method(Target targetObj, int i1, int i2);
	}
	
	static class Target {
		int sum(int i1, int i2) {
			return i1 +i2;
		}
	}
	
	private static void print(FunctionWithTarget func) {
		System.out.println(func.method(new Target(), 1, 2));
	}

}
