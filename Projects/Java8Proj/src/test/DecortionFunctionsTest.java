package test;

import java.awt.Color;
import java.util.function.Function;
import java.util.stream.Stream;

class Camera {
	private Color color;
	private Function<Color, Color> filter;

	public Camera(Color color) {
		this.color = color;
	}

	public void applyFilters(Function<Color, Color>... filters) {
		setFilters(filters);
		System.out.println(filter.apply(color));
	}

	private void setFilters(Function<Color, Color>... filters) {
		filter = Stream.of(filters).reduce(Function::andThen).orElse(color -> color);
	}
}

public class DecortionFunctionsTest {
	
	public static void main(String[] args) {
		Camera camera = new Camera(new Color(125, 125, 125));
		camera.applyFilters();
		camera.applyFilters(c -> c);
		camera.applyFilters(Color::brighter);
		camera.applyFilters(Color::darker);
		camera.applyFilters(Color::brighter, Color::darker);
	}
}
