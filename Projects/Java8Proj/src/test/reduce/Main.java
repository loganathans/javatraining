package test.reduce;

import java.util.Arrays;
import java.util.List;
import java.util.function.BinaryOperator;

public class Main {
	public static void main(String[] args) {
		List<Integer> list = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);

		List<Integer> list1 = Arrays.asList(0, 1, 2, 3, 4);
		List<Integer> list2 = Arrays.asList(5, 6, 7, 8, 9);

		// BinaryOperator<Integer> op = (i1, i2) -> i1 + i2;
		// BinaryOperator<Integer> op = (i1, i2) -> Integer.max(i1, i2);
		// BinaryOperator<Integer> op = (i1, i2) -> i1;
		//BinaryOperator<Integer> op = (i1, i2) -> i2;
		// BinaryOperator<Integer> op = (i1, i2) -> (i1 + i2) * (i1 + i2);
		BinaryOperator<Integer> op = (i1, i2) -> (i1 + i2) / 2;

		int reduction = reduce(list, 0, op);
		System.out.println("Reduction: " + reduction);

		// Simulation of parallel computing
		int reduction1 = reduce(list1, 0, op);
		int reduction2 = reduce(list2, 0, op);

		reduction = reduce(Arrays.asList(reduction1, reduction2), 0, op);
		System.out.println("Reduction parellel: " + reduction);

		
		
		List<Integer> ints = Arrays.asList(0, 1, 2, 3, 4, -1, -2, -3, -4);

		List<Integer> ints1 = Arrays.asList(0, 1, 2, 3, 4);
		List<Integer> ints2 = Arrays.asList(-1, -2, -3, -4);
		
		BinaryOperator<Integer> op1 = (i1, i2) -> Integer.max(i1, i2);

		int rdctn = reduce(ints, 0, op1);
		System.out.println("Rdctn: " + rdctn);

		// Simulation of parallel computing
		int rdctn1 = reduce(ints1, 0, op1);
		int rdctn2 = reduce(ints2, 0, op1);
		
		rdctn = reduce(Arrays.asList(rdctn1, rdctn2), 0, op1);
		System.out.println("Rdctn1 parellel: " + rdctn1);
		System.out.println("Rdctn2 parellel: " + rdctn2);
		System.out.println("Rdctn  parellel: " + rdctn);
	}

	private static int reduce(List<Integer> list, int identity, BinaryOperator<Integer> op) {
		int sum = identity;
		for (Integer i : list) {
			sum = op.apply(sum, i);
		}
		return sum;
	}
}
