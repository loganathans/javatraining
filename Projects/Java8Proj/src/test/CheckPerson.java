package test;

@FunctionalInterface
interface CheckPerson {
    boolean test(Person p);
}