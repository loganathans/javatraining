package test.predicate;

@FunctionalInterface
public interface Predicate<T> {
	public boolean test(T t);

	default public Predicate<T> and(Predicate<T> other) {
		return t -> this.test(t) && other.test(t);
	}
	
	default public Predicate<T> or(Predicate<T> other) {
		return t -> this.test(t) || other.test(t);
	}
	
	public static <U> Predicate<U> isEqualTo(U obj) {
		return t -> t.equals(obj);
	}
}
