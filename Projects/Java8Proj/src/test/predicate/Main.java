package test.predicate;

public class Main {

	public static void main(String[] args) {
		Predicate<String> p1 = s -> s.length() < 20;

		System.out.println("String is < 20 : " + p1.test("Hello"));

		Predicate<String> p2 = s -> s.length() > 5;

		System.out.println("String is > 5 : " + p2.test("Hello"));

		Predicate<String> p3 = p1.and(p2);

		System.out.println("String is > 5 and < 20 : " + p3.test("Hello"));

		Predicate<String> p4 = p1.or(p2);

		System.out.println("String is > 5 || < 20 : " + p4.test("Hello"));
		
		Predicate<String> p5 = Predicate.isEqualTo("Hello");

		System.out.println("Hello is Hello : " + p5.test("Hello"));
		
		System.out.println("Hello World is Hello : " + p5.test("Hello World"));
		
		Predicate<Integer> p6 = Predicate.isEqualTo(10);

		System.out.println("10 is 10 : " + p6.test(10));
		
		System.out.println("20 is 10 : " + p6.test(20));

	}

}
