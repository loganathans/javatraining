package test.collection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class MainCollection {

	public static void main(String[] args) {
		Person p1 = new Person("Gerard", 18);
		Person p2 = new Person("Albert", 66);
		Person p3 = new Person("Boby", 20);
		Person p4 = new Person("Natraj", 15);
		Person p5 = new Person("Karthy", 9);
		Person p6 = new Person("Gagan", 23);
		Person p7 = new Person("Akash", 30);
		Person p8 = new Person("Siva", 26);
		Person p9 = new Person("Siva", 22);

		List<Person> people = new ArrayList<>(Arrays.asList(p1, p2, p3, p4, p5, p6, p7, p8, p9));

		people.removeIf(person -> person.getAge() < 20);
		people.replaceAll(person -> new Person(person.getName().toUpperCase(), person.getAge()));
		people.sort(
				Comparator.comparing(Person::getName).thenComparing(Comparator.comparing(Person::getAge).reversed()));
		people.forEach(System.out::println);

	}

}
