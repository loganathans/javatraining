package test.collection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainMap {

	public static void main(String[] args) {
		Person p1 = new Person("Gerard", 18);
		Person p2 = new Person("Albert", 66);
		Person p3 = new Person("Boby", 20);
		Person p4 = new Person("Natraj", 15);
		Person p5 = new Person("Karthy", 9);
		Person p6 = new Person("Gagan", 23);
		Person p7 = new Person("Akash", 30);
		Person p8 = new Person("Siva", 26);
		Person p9 = new Person("Siva", 22);

		City chennai = new City("Chennai");
		City trichy = new City("Trichy");
		City madhurai = new City("Madhurai");
		City vellore = new City("Vellore");
		City selam = new City("Selam");

		Map<City, List<Person>> cityMap1 = new HashMap<>();
		cityMap1.putIfAbsent(vellore, new ArrayList<>());
		cityMap1.get(vellore).addAll(Arrays.asList(p1,p2,p6));
		cityMap1.computeIfAbsent(madhurai, city -> new ArrayList<>()).addAll(Arrays.asList(p2,p3));
		cityMap1.computeIfAbsent(madhurai, city -> new ArrayList<>()).addAll(Arrays.asList(p5,p7));
		cityMap1.computeIfAbsent(selam, city -> new ArrayList<>()).addAll(Arrays.asList(p5,p6));

		System.out.println("People from vellore: " + cityMap1.getOrDefault(vellore, Collections.EMPTY_LIST));
		System.out.println("People from madhurai: " + cityMap1.getOrDefault(madhurai, Collections.EMPTY_LIST));
		
		Map<City, List<Person>> cityMap2 = new HashMap<>();
		cityMap2.computeIfAbsent(chennai, city -> new ArrayList<>()).addAll(Arrays.asList(p1,p3,p5));
		cityMap2.computeIfAbsent(trichy, city -> new ArrayList<>()).addAll(Arrays.asList(p5,p7,p8));
		cityMap2.computeIfAbsent(selam, city -> new ArrayList<>()).addAll(Arrays.asList(p2,p4,p6));
		cityMap2.computeIfAbsent(vellore, city -> new ArrayList<>()).addAll(Arrays.asList(p2,p5));
		
		System.out.println("BEFORE MERGE");
		System.out.println("Map1");
		cityMap1.forEach((city, people) -> System.out.println(city + ": " + people));
		
		System.out.println("Map2");
		cityMap2.forEach((city, people) -> System.out.println(city + ": " + people));
		
		cityMap2.forEach((city, people) -> cityMap1.merge(city, people, (people1, people2) ->{
			people2.forEach(person -> {
				if(!people1.contains(person)) {
					people1.add(person);
				}
			});
			return people1;
		}));
		
		System.out.println("AFTER MERGE");
		System.out.println("Map1");
		cityMap1.forEach((city, people) -> System.out.println(city + ": " + people));
		
		System.out.println("Map2");
		cityMap2.forEach((city, people) -> System.out.println(city + ": " + people));
		
	}

}
