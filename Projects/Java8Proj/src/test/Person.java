package test;

import java.time.LocalDate;

public class Person {

    public enum Gender {
        MALE, FEMALE
    }

    public Person(String name, Gender gender, int age) {
        this.setName(name);
        this.gender = gender;
        this.age = age;
    }

    private String name;
    LocalDate birthday;
    private Gender gender;
    String emailAddress;
    private int age;

    public int getAge() {
        return age;
    }

    public void printPerson() {
        System.out.println(getName());
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getEmailAddress() {
        return getName() + "@domain.com";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return getName();
    }

    public int compareByName_instanceMethod(Person other) {
        return this.name.compareTo(other.name);
    }

    public static int compareByName_staticMethod(Person a, Person b) {
        return a.name.compareTo(b.name);
    }

    public int compareByNameNew(Person p3) {
        return 0;
    }

}