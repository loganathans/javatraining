package test;

class CheckPersonEligibleForSelectiveService implements CheckPerson {
    @Override
    public boolean test(Person p) {
        return p.getGender() == Person.Gender.MALE && p.getAge() >= 18 && p.getAge() <= 25;
    }
}