package test.stream;

import java.io.IOException;
import java.nio.file.Files;
import java.util.Spliterator;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import test.stream.model.Person;

public class CreatingSpliterator {
	public static void main(String[] args) {
		try(Stream<String> lines = Files.lines(Util.getPath("people.txt"))) {
			Spliterator<String> lineSpliterator = lines.spliterator();
			Spliterator<Person> personSplitarator = new PersonSpliterator(lineSpliterator);
			
			Stream<Person> personStream = StreamSupport.stream(personSplitarator, false);
			personStream.forEach(System.out::println);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
