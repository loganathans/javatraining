package test.stream;

import java.io.IOException;
import java.nio.file.Files;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CollectorsMain {

	public static void main(String[] args) throws IOException {
		// http://introcs.cs.princeton.edu/java/data/ - files location
		Set<String> shakespheareWords = Files.lines(Util.getPath("words.shakespeare.txt")).map(String::toLowerCase)
				.collect(Collectors.toSet());

		Set<String> scrabbleWords = Files.lines(Util.getPath("ospd.txt")).map(String::toLowerCase)
				.collect(Collectors.toSet());

		System.out.println("# of words of Shakespeare: " + shakespheareWords.size());
		System.out.println("# of words scrabble words: " + scrabbleWords.size());

		final int[] scableENScores =
				//a, b, c, d, e, f, g, h, i, h, k, l, m, n, o, p,  q, r, s, t, u, v, w, x, y,  z
				{ 1, 3, 3, 2, 1, 4, 2, 4, 1, 8, 5, 1, 3, 1, 1, 3, 10, 1, 1, 1, 1, 4, 4, 8, 4, 10 };

		Function<String, Integer> wordToScore = (word) -> word.chars().map(letter -> scableENScores[letter - 'a'])
				.sum();
		Map<Integer, List<String>> histoWordsByScore = shakespheareWords.stream().parallel().filter(scrabbleWords::contains)
				.collect(Collectors.groupingBy(wordToScore));
		System.out.println("# of histoWordsByScore: " + histoWordsByScore.size());

		histoWordsByScore.entrySet().stream().sorted(Comparator.comparing(entry -> -entry.getKey())).limit(3)
				.forEach(System.out::println);
		
		final int[] scableENDistributions =
				//a, b, c, d,  e, f, g, h, i, h, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z
				{ 9, 2, 2, 1, 12, 2, 3, 2, 9, 1, 1, 4, 2, 6, 8, 2, 1, 6, 4, 6, 4, 2, 2, 1, 2, 1 };
		
		Function<String, Map<Integer, Long>> histoWord = word -> word.chars().boxed()
				.collect(Collectors.groupingBy(c -> c, Collectors.counting()));
		
		Function<String, Long> nBlanks = word -> histoWord.apply(word) // Map<letter, # of letter>
				.entrySet().stream().parallel()
				.mapToLong(entry -> Long.max(entry.getValue() - (long) scableENDistributions[entry.getKey() - 'a'], 0L))
				.sum();
		
		System.out.println("# of blanks in whizzing: " + nBlanks.apply("whizzing"));
		
		Function<String, Integer> wordToScore2 = word -> histoWord.apply(word) // Map<letter, # of letter>
				.entrySet().stream().parallel()
				.mapToInt(entry -> scableENScores[entry.getKey() - 'a']
						* Integer.min(scableENDistributions[entry.getKey() - 'a'], entry.getValue().intValue()))
				.sum();
		
		System.out.println("score1 of whizzing: " + wordToScore.apply("whizzing"));
		System.out.println("score2 of whizzing: " + wordToScore2.apply("whizzing"));
		
		Map<Integer, List<String>> histoWordsByScore2 = shakespheareWords.stream()
				.parallel()
				.filter(scrabbleWords::contains)
				.filter(word -> nBlanks.apply(word) <= 2)
				.collect(Collectors.groupingBy(wordToScore2));
		System.out.println("# of histoWordsByScore2: " + histoWordsByScore2.size());

		histoWordsByScore2.entrySet().stream().sorted(Comparator.comparing(entry -> -entry.getKey())).limit(3)
				.forEach(System.out::println);
		
	}

}
