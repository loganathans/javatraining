package test.stream;

import java.io.File;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;

public final class Util {
	
	public static Path getPath(String name) {
		URL resource = CreatingSpliterator.class.getResource(name);
		Path path = Paths.get(new File(resource.getPath()).toURI());
		return path;
	}

}
