package test.stream;

import java.io.IOException;
import java.nio.file.Files;
import java.util.Comparator;
import java.util.IntSummaryStatistics;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.function.ToIntFunction;
import java.util.stream.Collectors;

public class IntStreamExamples {

	public static void main(String[] args) throws IOException {
		// http://introcs.cs.princeton.edu/java/data/ - files location
		Set<String> shakespheareWords = Files.lines(Util.getPath("words.shakespeare.txt")).map(String::toLowerCase)
				.collect(Collectors.toSet());

		Set<String> scrabbleWords = Files.lines(Util.getPath("ospd.txt")).map(String::toLowerCase)
				.collect(Collectors.toSet());

		System.out.println("# of words of Shakespeare: " + shakespheareWords.size());
		System.out.println("# of words scrabble words: " + scrabbleWords.size());

		final int[] scableENScores =
			   // a, b, c, d, e, f, g, h, i, h, k, l, m, n, o, p,  q, r, s, t, u, v, w, x, y,  z
				{ 1, 3, 3, 2, 1, 4, 2, 4, 1, 8, 5, 1, 3, 1, 1, 3, 10, 1, 1, 1, 1, 4, 4, 8, 4, 10 };

		Function<String, Integer> wordToScore = (word) -> word.chars().map(letter -> scableENScores[letter - 'a'])
				.sum();

		ToIntFunction<String> intScore = (word) -> word.chars().map(letter -> scableENScores[letter - 'a']).sum();

		System.out.println("Score of hello: " + intScore.applyAsInt("hello"));

		// OptionalInt maxScoreOfShakespeare =
		// shakespheareWords.stream().mapToInt(intScore).max();
		// System.out.println("Max score of Shakespeare: " +
		// maxScoreOfShakespeare.getAsInt());

		Optional<String> bestWord = shakespheareWords.stream().filter(word -> scrabbleWords.contains(word))
				.max(Comparator.comparing(wordToScore));
		System.out.println("Best word" + bestWord.orElse("None"));

		Optional<String> bestWord1 = shakespheareWords.stream().filter(scrabbleWords::contains)
				.max(Comparator.comparingInt(intScore));
		System.out.println("Best word" + bestWord1.orElse("None"));
		
		IntSummaryStatistics summaryStatistics = shakespheareWords.stream().mapToInt(intScore).summaryStatistics();
		System.out.println("Summary stats: " + summaryStatistics);
		
		IntSummaryStatistics summaryStatisticsParallel = shakespheareWords.stream().parallel().mapToInt(intScore).summaryStatistics();
		System.out.println("Summary stats parallel: " + summaryStatisticsParallel);

	}

}
