package test.stream;

import java.io.IOException;
import java.nio.file.Files;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class FlatmapExamples {

	public static void main(String[] args) throws IOException {
		// http://introcs.cs.princeton.edu/java/data/ - files location
		Stream<String> stream1 = Files.lines(Util.getPath("TomSawyer.txt"));
		Stream<String> stream2 = Files.lines(Util.getPath("aesop.txt"));
		Stream<String> stream3 = Files.lines(Util.getPath("amendments.txt"));
		Stream<String> stream4 = Files.lines(Util.getPath("manifesto.txt"));
		Stream<String> stream5 = Files.lines(Util.getPath("muchado.txt"));

		// System.out.println("Stream1 count: " + stream1.count());
		// System.out.println("Stream2 count: " + stream2.count());
		// System.out.println("Stream3 count: " + stream3.count());
		// System.out.println("Stream4 count: " + stream4.count());
		// System.out.println("Stream5 count: " + stream5.count());

		Stream<Stream<String>> streamOfStreams = Stream.of(stream1, stream2, stream3, stream4, stream5);

		// System.out.println("Stream of Streams count: " +
		// streamOfStreams.count());

		Stream<String> streamOfLines = streamOfStreams.flatMap(Function.identity());

		// System.out.println("Lines count: " + streamOfLines.count());

		Function<String, Stream<String>> lineToWords = (line) -> Pattern.compile(" ").splitAsStream(line);
		Stream<String> wordsStream = streamOfLines.flatMap(lineToWords).distinct().map(word -> word.toLowerCase())
				.filter(word -> word.length() == 4);
		System.out.println("Words count: " + wordsStream.count());
	}

}
