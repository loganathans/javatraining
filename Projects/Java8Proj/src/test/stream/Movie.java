package test.stream;

import java.util.HashSet;
import java.util.Set;

import test.stream.model.Actor;

public class Movie {
	
	private final String title;
	private final int releaseYear;
	
	public Movie(String title, int releaseYear) {
		super();
		this.title = title;
		this.releaseYear = releaseYear;
	}

	private Set<Actor> actors = new HashSet<>();
	
	public String title() {
		return title;
	}
	
	public int releaseYear() {
		return releaseYear;
	}
	
	public void addActor(Actor actor) {
		if(!actors.contains(actor)) {
			actors.add(actor);
		}
	}
	
	public Set<Actor> actors() {
		return actors;
	}

	@Override
	public String toString() {
		return "Movie [title=" + title + ", releaseYear=" + releaseYear + ", actors=" + actors + "]";
	}
	
	

}
