package test.stream;

import java.io.IOException;
import java.nio.file.Files;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collector.Characteristics;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import test.stream.model.Actor;

public class CustomCollectorsMain {

	public static void main(String[] args) throws IOException {
		// http://introcs.cs.princeton.edu/java/data/ - files location
		Stream<String> lines = Files.lines(Util.getPath("movies-mpaa.txt"));
		
		Stream<Movie> moviesStream = StreamSupport.stream(new MoviesSpliterator(lines.spliterator()), false);

		Set<Movie> movies = moviesStream.collect(Collectors.toSet());
		lines.close();
		System.out.println("# movies: " + movies.size());

		// # of actors
		long nActors = movies.stream().flatMap(movie -> movie.actors().stream()).distinct().count();

		System.out.println("# of actors: " + nActors);

		// actor that played in greatest # of movies
		Map<Actor, Long> actorsNMoviesCounts = movies.stream().flatMap(movie -> movie.actors().stream())
				.collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

//		List<Entry<Actor, Long>> mostPlayedActors = actorsNMoviesCounts.entrySet().stream()
//				.sorted(Comparator.comparing(entry -> -entry.getValue())).limit(5).collect(Collectors.toList());
		  
		Entry<Actor, Long> mostPlayedActor = actorsNMoviesCounts.entrySet().stream().parallel()
				.max(Entry.comparingByValue()).get();

		System.out.println("mostPlayedActor: " + mostPlayedActor);
		
		// actor that played in greatest # of movies in each year
		// Map<year of release, Map<Actor, # of Movies in that year>>
		Map<Integer, HashMap<Actor, AtomicLong>> yearsNActorMovieNumbers = movies.stream().parallel()
				.collect(Collectors.groupingBy(
							movie -> movie.releaseYear(),
							Collector.of(
									() -> new HashMap<Actor, AtomicLong>(), 
									(map, movie) -> {
										movie.actors()
											.forEach(
													actor -> {
														map.computeIfAbsent(actor, ac -> new AtomicLong()).incrementAndGet();
													}
											);
									},
									(map1, map2) -> {
										map2.entrySet().forEach(entry -> {
											//map1.computeIfAbsent(entry.getKey(), ac -> new AtomicLong()).addAndGet(entry.getValue().get());
											map1.merge(
													entry.getKey(), 
													entry.getValue(), 
													(val1, val2) -> {
														val1.addAndGet(val2.get());
														return val1;
													}
											);
										});
										return map1;
									}, 
									Characteristics.IDENTITY_FINISH
									)
							)
						);

		Map<Integer, Entry<Actor, AtomicLong>> yearsNActorInMostMovieCount = yearsNActorMovieNumbers.entrySet()
				.stream().parallel()
				.collect(Collectors.toMap(
						entry -> entry.getKey(),
						entry -> entry.getValue().entrySet().stream().parallel()
								.max(Entry.comparingByValue(Comparator.comparing(al -> al.get()))).get()

		));
		
		yearsNActorInMostMovieCount.entrySet().forEach(System.out::println);
		System.out.println("Most acted actor in year 2000 :" + yearsNActorInMostMovieCount.get(2000));
		
		// actor that played in greatest # of movies in a year all time
		Entry<Integer, Entry<Actor, AtomicLong>> mostActedMovieInAYearForAllTime = yearsNActorInMostMovieCount.entrySet().stream().max(Entry
				.comparingByValue(Entry.comparingByValue(Comparator.comparing(al -> al.get())))).get();
		
		System.out.println("Most acted actor in year in all time :" + mostActedMovieInAYearForAllTime);

	}

}
