package test.stream;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.stream.Stream.Builder;

public class MainStream {

	public static void main(String[] args) {
		List<Integer> ints = Arrays.asList(0, 1, 2, 3, 4, 5);
		Stream<Integer> stream = ints.stream();
		Stream<Integer> stream2 = Stream.of(0, 1, 2, 3, 4, 5);

		stream.forEach(MainStream::print);
		stream2.skip(2).forEach(MainStream::print);

		Stream<String> stream3 = Stream.generate(() -> "one");
		stream3.limit(5).forEach(MainStream::print);

		AtomicInteger atomicInt = new AtomicInteger(0);
		IntStream stream31 = IntStream.generate(() -> atomicInt.incrementAndGet());
		stream31.limit(5).forEach(MainStream::print);

		IntStream stream32 = IntStream.rangeClosed(1, 5);
		stream32.forEach(MainStream::print);

		Stream<String> stream4 = Stream.iterate("+", s -> s + "+");
		stream4.skip(2).limit(5).forEach(MainStream::print);

		IntStream intStream = ThreadLocalRandom.current().ints();
		intStream.limit(4).forEach(MainStream::print);

		ints.stream().filter(i -> i % 2 == 0).map(i -> i * i).forEach(MainStream::print);

		Optional<Integer> optional = Stream.iterate("+", s -> s + "+").limit(10).map(s -> s.length())
				.filter(i -> i % 2 == 1).peek(MainStream::print).reduce((i1, i2) -> i1 + i2);
		System.out.println("Sum:" + optional.get());

		Builder<Integer> builder = Stream.<Integer>builder().add(0).add(1).add(2);
		IntStream.rangeClosed(1, 5).forEach(val -> builder.accept(-val));
		Stream<Integer> stream5 = builder.build();
		stream5.forEach(MainStream::print);
	}

	private static <T> void print(T t) {
		System.out.println(t.toString());
	}

}
