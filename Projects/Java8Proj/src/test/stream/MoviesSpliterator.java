package test.stream;

import java.util.Spliterator;
import java.util.function.Consumer;

import test.stream.model.Actor;

public class MoviesSpliterator implements Spliterator<Movie> {

	private Spliterator<String> linesSpliterator;
	private Movie movie;

	public MoviesSpliterator(Spliterator<String> linesSpliterator) {
		this.linesSpliterator = linesSpliterator;
	}

	@Override
	public boolean tryAdvance(Consumer<? super Movie> action) {
		Consumer<? super String> lineConsumer = line -> {
				String[] elements = line.split("/");
				String title = elements[0].substring(0, elements[0].lastIndexOf("("));
				String releaseYear = elements[0].substring(elements[0].lastIndexOf("(") + 1, (elements[0].lastIndexOf(")")));
				if(releaseYear.contains(",")) {
					releaseYear = releaseYear.substring(0, (releaseYear.lastIndexOf(",")));
				}
//				if (releaseYear.contains(",")) {
//					// skip movies with a coma in their title
//					return;
//				}

				movie = new Movie(title, Integer.parseInt(releaseYear));
				for (int i = 1; i < elements.length; i++) {
					String[] name = elements[i].split(",");
					String lastName = name[0].trim();
					String firstName = "";

					if (name.length > 1) {
						firstName = name[1].trim();
					}

					movie.addActor(new Actor(lastName, firstName));
				}
		};
		if(linesSpliterator.tryAdvance(lineConsumer)) {
			action.accept(movie);
			return true;
		}
		return false;
	}

	@Override
	public Spliterator<Movie> trySplit() {
		return null;
	}

	@Override
	public long estimateSize() {
		return linesSpliterator.estimateSize();
	}

	@Override
	public int characteristics() {
		return linesSpliterator.characteristics();
	}

}
