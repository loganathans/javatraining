package test.stream;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;

public class ReduceTest {
	public static void main(String[] args) {
		List<Integer> intList = Arrays.asList(new Integer[] {1,2,3,4,5,6,7,8,9,10});
		
		intList.stream().mapToInt(e -> e).reduce((a,b) -> a+b).ifPresent(System.out::println);
		
		print(intList.stream().reduce(0, (a, b) -> Math.max(a, b)));
		
		print(intList.stream().reduce(1, (a, b) -> a * b));
		
		print(intList.stream().map(e -> new BigInteger("" + e)).reduce((a, b) -> {
			BigInteger res = a.add(b).divide(new BigInteger("" + 2));
			System.out.println("a=" + a + " b=" + b + " res: " + res);
			return res;
		}));
		
		class Box {
			int i;
			int val;
			public Box(int i, int val) {
				super();
				this.i = i;
				this.val = val;
			}
			@Override
			public String toString() {
				return "Box [i=" + i + ", val=" + val + "]";
			}
			
		}
		
		print(intList.stream().map(val -> new Box(0, val)).reduce(new Box(0,0),(a,b) -> new Box(a.i + 1, a.val + b.val)));
		
		print(intList.stream().map(val -> new Box(0, val)).reduce(new Box(0,0),(a,b) -> {
			if(a.i < intList.size() / 2) {
				return new Box(a.i + 1, a.val + b.val);
			} else {
				return a;
			}
		}));
		
		print(intList.stream().reduce("", (s, val) ->  s + val, (s1,s2) -> s1 + s2));
		

		

		
		

	}

	private static void print(Object obj) {
		System.out.println(obj);
	}

}
