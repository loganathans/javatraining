package test.stream;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ParellelStreamsExample {

	public static void main(String[] args) {
		System.setProperty("java.util.concurrent.ForkJoinPool.common.parallelism", "2");
		
		//List<String> list = new ArrayList<>(); //no thread aware
		//List<String> list = new CopyOnWriteArrayList<>();
		
		List<String> list = Stream.iterate("+", s -> s + "+")
		.parallel()
		.limit(1000)
		//.peek(s -> System.out.println(s + " - in thread " + Thread.currentThread().getName()))
		//.forEach(s -> list.add(s)); // not recommended
		.collect(Collectors.toList());
		
		System.out.println("# of list: " + list.size());
	}

}
