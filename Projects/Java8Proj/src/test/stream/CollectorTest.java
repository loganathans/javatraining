package test.stream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collector.Characteristics;
import java.util.stream.Collectors;

public class CollectorTest {
	
	static class Box<T,R> {
		T val1;
		R val2;
		
		public Box(T t, R r) {
			super();
			this.val1 = t;
			this.val2 = r;
		}

		@Override
		public String toString() {
			return "Box [t=" + val1 + ", r=" + val2 + "]";
		}
		
		
	}

	public static void main(String[] args) {
		List<Integer> intList = Arrays.asList(new Integer[] {1,2,3,4,5,6,7,8,9,10});
		
		Box<Map<Integer, Integer>, Integer> res = intList.stream().collect(() -> new Box<Map<Integer, Integer>, Integer>(new LinkedHashMap<Integer, Integer>(), 0), 
		(box, val) -> {
			Map<Integer, Integer> map = box.val1;
			Integer index = box.val2;
			if(index < (intList.size() / 2)) {
				map.computeIfAbsent(val, k -> 0);
			}
			map.forEach((k,v) -> {
				map.computeIfPresent(k, (k1,v1) -> {
					return val % k1 == 0? v1 + 1: v1;
				});
			});
			
			box.val2++;
			
		}, (b1 , b2) -> {
			//Nothing to do. Expecting sequential invocation only
		});
		
		System.out.println(res.val1);
		
		Collector<Integer, Box<Map<Integer, Integer>, Integer>, Box<Map<Integer, Integer>, Integer>> modulusCountCollector = Collector.<Integer,Box<Map<Integer, Integer>, Integer>>of(() -> new Box<Map<Integer, Integer>, Integer>(new LinkedHashMap<Integer, Integer>(), 0), 
				(box, val) -> {
					Map<Integer, Integer> map = box.val1;
					Integer index = box.val2;
					if(index < (intList.size() / 2)) {
						map.computeIfAbsent(val, k -> 0);
					}
					map.forEach((k,v) -> {
						map.computeIfPresent(k, (k1,v1) -> {
							return val % k1 == 0? v1 + 1: v1;
						});
					});
					
					box.val2++;
					
				},
				(b1 , b2) -> b1, 
				Characteristics.IDENTITY_FINISH);
		
		Box<Map<Integer, Integer>, Integer> box = intList.stream().collect(modulusCountCollector);
		
		System.out.println(box.val1);
		
		Map<Integer, Integer> resMap = intList.stream().collect(Collectors.collectingAndThen(modulusCountCollector, box1 -> box1.val1));
		System.out.println(resMap);
		
		Double average = intList.stream().collect(Collectors.averagingInt(e -> e));
		System.out.println(average);
		
		print(intList.stream().collect(Collectors.maxBy(Comparator.comparing(Function.identity()))));
		
		print(intList.stream().collect(Collectors.minBy(Comparator.comparing(Function.identity()))));
		
		print(intList.stream().collect(Collectors.reducing((a,b ) -> a + b)));
		
		AtomicInteger index1 = new AtomicInteger(0);
		
		Collector<Integer, ?, HashMap<Integer, Integer>> modulusCollector2 = Collectors.mapping(val -> new Box<Integer,Integer>(val, index1.getAndIncrement()), 
		Collector.of(() -> new HashMap<Integer, Integer>(), 
				(map, bx) -> {
					if(bx.val2 < intList.size() / 2) {
						map.computeIfAbsent(bx.val1, k -> 0);
					}
					
					map.forEach((k,v) -> {
						if(bx.val1 % k == 0) {
							map.computeIfPresent(k, (k1, v1) -> v1 + 1);
						}
					});
				}, 
				(map1, map2) -> map1,
				Characteristics.IDENTITY_FINISH)
		);
		print(intList.stream().collect(Collectors.collectingAndThen(modulusCollector2, map -> map.values())));
		
		AtomicInteger index2 = new AtomicInteger(0);

		
		Collector<Box<Integer, Integer>, HashMap<Integer, List<Integer>>, HashMap<Integer, List<Integer>>> divisablesListCollector = Collector.of(() -> new HashMap<Integer, List<Integer>>(), 
				(map, bx) -> {
					if(bx.val2 < intList.size() / 2) {
						map.computeIfAbsent(bx.val1, k -> new ArrayList<>());
					}
					
					map.forEach((k,v) -> {
						if(bx.val1 % k == 0) {
							map.get(k).add(bx.val1);
						}
					});
				}, 
				(map1, map2) -> map1,
				Characteristics.IDENTITY_FINISH);
		Collector<Integer, ?, HashMap<Integer, List<Integer>>> divisablesCollector = Collectors.mapping(val -> new Box<Integer,Integer>(val, index2.getAndIncrement()), 
				divisablesListCollector);
		
		print(intList.stream().collect(Collectors.collectingAndThen(divisablesCollector, map -> map.entrySet())));
		
		
		AtomicInteger index3 = new AtomicInteger(0);

		Collector<Integer, ?, HashMap<Integer, List<Integer>>> divisablesCollector2 = Collectors.mapping(val -> new Box<Integer,Integer>(val, index3.getAndIncrement()), 
				divisablesListCollector);

		print(
				intList.stream().collect(Collectors.collectingAndThen(divisablesCollector2, 
						map -> map.entrySet().stream().collect(Collectors.toMap(Entry::getKey, e -> e.getValue().size()))))
				);


		
	}
	
	private static void print(Object obj) {
		System.out.println(obj);
	}

}
