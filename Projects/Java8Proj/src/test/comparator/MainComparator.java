package test.comparator;

import java.util.function.Function;

public class MainComparator {

	public static void main(String[] args) {
		Comparator<Person> cmpAge1 = (p1, p2) -> p1.getAge() - p2.getAge();
		Comparator<Person> cmpFirstName1 = (p1, p2) -> p1.getFirstName().compareTo(p2.getFirstName());
		Comparator<Person> cmpLastName1= (p1, p2) -> p1.getLastName().compareTo(p2.getLastName());
		
		Function<Person, Integer> f1 = p -> p.getAge();
		Function<Person, String> f2 = p -> p.getFirstName();
		Function<Person, String> f3 = p -> p.getLastName();
		
		Comparator<Person> cmpAge = Comparator.comparing(Person::getAge);
		Comparator<Person> cmpFirstName = Comparator.comparing(Person::getFirstName);
		Comparator<Person> cmpLastName = Comparator.comparing(Person::getLastName);
		
		Comparator<Person> cmp =  Comparator.comparing(Person::getAge).thenComparing(Person::getLastName).thenComparing(Person::getFirstName);
		
		
		
		
	}

}
