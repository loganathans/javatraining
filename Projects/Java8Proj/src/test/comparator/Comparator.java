package test.comparator;

import java.util.function.Function;

@FunctionalInterface
public interface Comparator<T> {

	public int compare(T t1, T t2);

	public static <U> Comparator<U> comparing(Function<U, Comparable> f) {
		return (u1, u2) -> f.apply(u1).compareTo(f.apply(u2));
	}

	default public Comparator<T> thenComparing(Comparator<T> cmpFirstName) {
		return (t1, t2) -> {
			int result = compare(t1, t2);
			return result == 0 ? cmpFirstName.compare(t1, t2) : result;
		};
	}

	default public Comparator<T> thenComparing(Function<T, Comparable> f) {
		return thenComparing(comparing(f));
	}

}
