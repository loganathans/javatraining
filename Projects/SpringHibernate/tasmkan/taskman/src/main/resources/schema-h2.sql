CREATE TABLE task (
    tid   VARCHAR(10) NOT NULL,
    desc VARCHAR(30) NOT NULL,
    pid VARCHAR(10) NOT NULL,
    start_date DATE,
    due_date DATE,
    PRIMARY KEY (tid)
);

CREATE TABLE employee (
    mid   VARCHAR(10)  NOT NULL,
    name VARCHAR(30) NOT NULL,
    PRIMARY KEY (mid)
);

CREATE TABLE project (
    pid   VARCHAR(10)  NOT NULL,
    name VARCHAR(30) NOT NULL,
    PRIMARY KEY (pid)
);

ALTER TABLE task
    ADD FOREIGN KEY (pid) REFERENCES project(pid);

CREATE TABLE task_employee_mapping (
    tid   VARCHAR(10)  NOT NULL,
    mid VARCHAR(10) NOT NULL,
    PRIMARY KEY (tid, mid)
);

ALTER TABLE task_employee_mapping 
	ADD FOREIGN KEY (tid) REFERENCES task(tid);
ALTER TABLE task_employee_mapping 
    ADD FOREIGN KEY (mid) REFERENCES employee(mid);

CREATE TABLE project_employee_mapping (
    pid   VARCHAR(10)  NOT NULL,
    mid VARCHAR(10) NOT NULL,
    PRIMARY KEY (pid, mid)
);    

ALTER TABLE project_employee_mapping 
	ADD FOREIGN KEY (pid) REFERENCES project(pid);
ALTER TABLE project_employee_mapping 
    ADD FOREIGN KEY (mid) REFERENCES employee(mid);