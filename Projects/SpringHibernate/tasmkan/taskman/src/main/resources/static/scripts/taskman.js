var taskmanApp = angular.module('taskmanApp', ['ngRoute', '720kb.datepicker']);

taskmanApp.config(function($routeProvider) {
	$routeProvider.when('/home', {
		templateUrl:'home.html',
		controller: 'homeController'
	}).when('/addTask',{
		templateUrl:'addTask.html',
		controller: 'addTaskController'
	}).when('/viewTasks', {
		templateUrl:'viewTasks.html',
		controller: 'viewTasksController'
	}).otherwise('/home');
});

taskmanApp.controller('homeController', function($scope) {
	$scope.message= 'Contact Message';
});

taskmanApp.controller('addTaskController',['$scope', '$http', '$location', function($scope, $http, $location) {
	$scope.newTask = {};
	 	 	
 	$http.get("/taskman/projects").success(function(data){
 		$scope.projects = data.data;
 	});
 	
 	$scope.selectedProjectChanged = function(){
 	    $scope.newTask.pid =  $scope.selectedProject.pid;
 	    $http.get("/taskman/employees/" + $scope.selectedProject.pid).success(function(data){
	 		$scope.employeesForProject = data.data;
	 	});
 	}
 	
 	$scope.selectedEmployeesChanged = function(){
 	    $scope.newTask.employees =  $scope.selectedEmployees;
 	}
 	
 	$scope.goToHome = function() {
	 	$location.path('/home');
 	}
 	
 	$scope.addNewTask = function() {
 		$http({
 	        method: 'POST',
 	        url: '/taskman/task',
 	        data: $scope.newTask,
 	        headers: {'Content-Type': 'application/json', 'Accept':'application/json'}
 	    }).then(function(result) {
 	    	alert(result.data.data);
 			$scope.goToHome();
        }, function(error) {
        	var msg = error.data.errors.map (err => err.field + ' - ' + err.message).join('/n');
        	alert(msg);
        });
 	}
 	$scope.isValidNewTask = function() {
 		return $scope.newTask 
 				&& $scope.newTask.pid 
 				&& $scope.newTask.desc 
 				&& $scope.newTask.startDate 
 				&& $scope.newTask.dueDate
 				&& $scope.newTask.employees
 				&& $scope.newTask.employees.length > 0
 				&& !$scope.startDateError
 				&& !$scope.dueDateError;
 	}
 	
 	 
 	  $scope.$watch('startDateStr', function (value) {
 	    $scope.startDateError = false;
 	    if(value && value.length > 0) {
 	    	try {
 	 	    	var date_regex = /^(0[1-9]|1\d|2\d|3[01])-(0[1-9]|1[0-2])-(19|20)\d{2}$/ ;
 	 	    	if(date_regex.test(value)) {
  	 	    		$scope.newTask.startDate = moment(value + 'Z', "DD-MM-YYYYZ");
  	 	    	} else {
  	 	    		$scope.newTask.startDate = null;
  	 	    	}
 	 	    } catch(e) {
 	 	    	$scope.newTask.startDate = false;
 	 	    }
 	 	 
 	 	    if (!$scope.newTask.startDate) {
 	 	      $scope.startDateError = "This is not a valid date";
 	 	    }
 	    }
 	  });
 	  
 	 $scope.$watch('dueDateStr', function (value) {
  	    $scope.dueDateError = false;
  	    if(value && value.length > 0) {
  	    	try {
  	 	    	var date_regex = /^(0[1-9]|1\d|2\d|3[01])-(0[1-9]|1[0-2])-(19|20)\d{2}$/ ;
  	 	    	if(date_regex.test(value)) {
  	 	    		$scope.newTask.dueDate = moment(value + 'Z', "DD-MM-YYYYZ");
  	 	    	} else {
  	 	    		$scope.newTask.dueDate = null;
  	 	    	}
  	 	    } catch(e) {
  	 	    	$scope.newTask.dueDate = false;
  	 	    }
  	 	 
  	 	    if (!$scope.newTask.dueDate) {
  	 	      $scope.dueDateError = "This is not a valid date";
  	 	    }
  	    }
  	 });
 	  
}]);

taskmanApp.controller('viewTasksController',['$scope', '$http', '$location', function($scope, $http, $location) {

	
	$http.get("/taskman/projects").success(function(data){
		var first = {"pid" : "all", "name" : "All Projects"}
		data.data.unshift(first);
 		$scope.projects = data.data;
 	});
	
	$scope.selectedProjectChanged = function(){
 	    $http.get("/taskman/tasks/" + $scope.selectedProject.pid).success(function(data){
	 		$scope.tasksForProject = data.data;
 	    	var selProjects = [];
			for (i = 0; i < data.data.length; i++) {
					for (j = 0; j < $scope.projects.length; j++) {
					var proj = $scope.projects[j];
					if (proj.pid !== 'all' 
						&& proj.pid === data.data[i].pid
						&& selProjects.filter(e => e.pid === proj.pid).length == 0
					) {
						selProjects.push(proj);
						break;
					}
				}
			}
	 		$scope.selectedProjects = selProjects;
	 	});
 	}
	
	$scope.isTaskForProject = function(taskId, projId) {
 		return taskId === projId;
 	}
	
	$scope.formatDate = function(dateStr) {
		var date = new Date(dateStr);
		return date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear();
	}
	
}]);

taskmanApp.filter('filterTaskForProject', function() {  
	   return function(tasks, projId) {  
	      return tasks.filter(task => task.pid === projId);  
	   };  
	 });  