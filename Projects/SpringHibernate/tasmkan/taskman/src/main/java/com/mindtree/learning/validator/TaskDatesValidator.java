package com.mindtree.learning.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.SmartValidator;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.mindtree.learning.dto.TaskRequestDTO;

@Component
public class TaskDatesValidator implements Validator {
	
	@Autowired
	private SmartValidator validator;

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.isAssignableFrom(TaskRequestDTO.class);
	}

	@Override
	public void validate(Object target, Errors errors) {
		if(target instanceof TaskRequestDTO) {
			ValidationUtils.invokeValidator(validator, target, errors);
			TaskRequestDTO taskRequestDTO = (TaskRequestDTO) target;
			if(!errors.hasErrors()
					&& taskRequestDTO.getStartDate().isAfter(taskRequestDTO.getDueDate())) {
				errors.rejectValue("dueDate", "duedate.error");
			}
		}
	}

}
