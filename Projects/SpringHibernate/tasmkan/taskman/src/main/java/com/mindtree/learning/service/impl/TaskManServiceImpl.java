package com.mindtree.learning.service.impl;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.mindtree.learning.dto.EmployeeDTO;
import com.mindtree.learning.dto.ProjectDTO;
import com.mindtree.learning.dto.ResponseDTO;
import com.mindtree.learning.dto.TaskRequestDTO;
import com.mindtree.learning.dto.TaskResponseDTO;
import com.mindtree.learning.entity.EmployeeEntity;
import com.mindtree.learning.entity.ProjectEntity;
import com.mindtree.learning.entity.TaskEntity;
import com.mindtree.learning.exception.TaskManAppException;
import com.mindtree.learning.repo.EmployeeRepository;
import com.mindtree.learning.repo.ProjectRepository;
import com.mindtree.learning.repo.TaskRepository;
import com.mindtree.learning.spi.TaskManService;

@Service
public class TaskManServiceImpl implements TaskManService {
	
    private static final String TASK_ID_PREFIX = "T";


	private static final Logger LOGGER = Logger.getLogger(TaskManServiceImpl.class.getName());

	
	@Autowired
	private TaskRepository taskRepo;

	@Autowired
	private ProjectRepository projectRepo;

	@Autowired
	private EmployeeRepository employeeRepo;

	@Autowired
	private ModelMapper modelMapper;

	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	private MessageSource messageSource;

	private AtomicInteger newTaskCounter = new AtomicInteger(0);

	@Override
	public ResponseDTO<List<ProjectDTO>> getProjects() throws TaskManAppException{
		try {
			List<ProjectDTO> projects = projectRepo.findAll()
					.stream()
					.map(entity -> modelMapper.map(entity, ProjectDTO.class))
					.collect(Collectors.toList());
			return new ResponseDTO<>(projects);
		} catch (Exception e) {
			String errorCode = "internal.error";
			LOGGER.log(Level.SEVERE, messageSource.getMessage(errorCode, null, null), e);
			String error = messageSource.getMessage(errorCode, null, null);
			throw new TaskManAppException(error, e);
		}
	}

	@Override
	public ResponseDTO<List<EmployeeDTO>> getEmployees(String pid) throws TaskManAppException {
		try {
			List<EmployeeDTO> empList = employeeRepo.findAllByProjectsPid(pid)
						.stream()
						.map(entity -> modelMapper.map(entity, EmployeeDTO.class))
					.collect(Collectors.toList());
			return new ResponseDTO<>(empList);
		} catch (Exception e) {
			String errorCode = "internal.error";
			LOGGER.log(Level.SEVERE, messageSource.getMessage(errorCode, null, null), e);
			String error = messageSource.getMessage(errorCode, null, null);
			throw new TaskManAppException(error, e);
		}
	}

	@Override
	public ResponseDTO<String> addTask(TaskRequestDTO taskRequestDTO) throws TaskManAppException {
		try {
			if (taskRequestDTO.getTid() == null) {
				taskRequestDTO.setTid(TASK_ID_PREFIX + newTaskCounter.incrementAndGet());
			}
			addTaskToRepo(taskRequestDTO);
		} catch (Exception e) {
			String errorCode = "task.addition.failed";
			LOGGER.log(Level.SEVERE, messageSource.getMessage(errorCode, null, null), e);
			String error = messageSource.getMessage(errorCode, null, null);
			throw new TaskManAppException(error, e);
		}

		String message = messageSource.getMessage("task.added", null, null) + taskRequestDTO.getTid() + " - " + taskRequestDTO.getDesc();
		return new ResponseDTO<>(message);
	}

	@Transactional
	private void addTaskToRepo(TaskRequestDTO taskRequestDTO) {
		TaskEntity taskEntity = modelMapper.map(taskRequestDTO, TaskEntity.class);
		taskEntity.getEmployees().clear();
		ProjectEntity projectEntity = entityManager.getReference(ProjectEntity.class, taskRequestDTO.getPid());
		taskEntity.setProject(projectEntity);

		List<EmployeeEntity> employeeEntities = taskRequestDTO.getEmployees().stream()
				.map(mid -> entityManager.getReference(EmployeeEntity.class, mid))
				.collect(Collectors.toList());
		taskEntity.getEmployees().addAll(employeeEntities);

		taskRepo.save(taskEntity);
	}

	@Override
	public ResponseDTO<List<TaskResponseDTO>> getTasks(String pid) throws TaskManAppException {
		try {
			List<TaskEntity> taskEntities;
			if (pid == null) {
				taskEntities = taskRepo.findAll();
			} else {
				taskEntities = taskRepo.findAllByProjectPid(pid);
			}
	
			List<TaskResponseDTO> tasks = taskEntities.stream()
					.map(entity -> {
						TaskResponseDTO taskResponseDTO = modelMapper.map(entity, TaskResponseDTO.class);
						taskResponseDTO.setPid(entity.getProject().getPid());
						return taskResponseDTO;
					})
					.collect(Collectors.toList());
			return new ResponseDTO<>(tasks);
		} catch (Exception e) {
			String errorCode = "internal.error";
			LOGGER.log(Level.SEVERE, messageSource.getMessage(errorCode, null, null), e);
			String error = messageSource.getMessage(errorCode, null, null);
			throw new TaskManAppException(error, e);
		}
	}

}
