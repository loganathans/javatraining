package com.mindtree.learning.entity;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "employee")
public class EmployeeEntity implements Comparable<EmployeeEntity>{

	@Id
	private String mid;
	private String name;
	
	@ManyToMany(mappedBy="employees")
	private Collection<TaskEntity> tasks = new ArrayList<>();
	
	@ManyToMany(mappedBy="employees")
	private Collection<ProjectEntity> projects = new ArrayList<>();

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((mid == null) ? 0 : mid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmployeeEntity other = (EmployeeEntity) obj;
		if (mid == null) {
			if (other.mid != null)
				return false;
		} else if (!mid.equals(other.mid))
			return false;
		return true;
	}

	@Override
	public int compareTo(EmployeeEntity o) {
		return this.mid.compareToIgnoreCase(o.mid);
	}
	
}
