package com.mindtree.learning.entity;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "project")
public class ProjectEntity implements Comparable<ProjectEntity>{

	@Id
	private String pid;
	private String name;
	
	@ManyToMany(cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
	@JoinTable(
			name = "project_employee_mapping",
			joinColumns = @JoinColumn(name="pid"),
			inverseJoinColumns=@JoinColumn(name="mid")
	)
	private Collection<EmployeeEntity> employees = new ArrayList<>();
	
	@OneToMany(mappedBy="project")
	private Collection<TaskEntity> tasks = new ArrayList<>();

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pid == null) ? 0 : pid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProjectEntity other = (ProjectEntity) obj;
		if (pid == null) {
			if (other.pid != null)
				return false;
		} else if (!pid.equals(other.pid))
			return false;
		return true;
	}

	@Override
	public int compareTo(ProjectEntity o) {
		return this.pid.compareToIgnoreCase(o.pid);
	}
	
}
