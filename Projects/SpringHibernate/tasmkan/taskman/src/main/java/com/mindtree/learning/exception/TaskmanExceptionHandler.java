package com.mindtree.learning.exception;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.mindtree.learning.dto.ErrorResponseDTO;
import com.mindtree.learning.dto.FieldError;
import com.mindtree.learning.service.impl.TaskManServiceImpl;

@RestControllerAdvice
public class TaskmanExceptionHandler {
	
	@Autowired
	private MessageSource messageSource;
	
    private static final Logger LOGGER = Logger.getLogger(TaskManServiceImpl.class.getName());

	@ExceptionHandler
	public ResponseEntity<ErrorResponseDTO<?>> handleException(TaskManAppException e) {
		String errMessage = e.getMessage() == null ? messageSource.getMessage("internal.error", null, null) : e.getMessage();
		LOGGER.log(Level.SEVERE, errMessage, e);
		if(e.getCause() instanceof TaskManValidationException) {
			return handleException((TaskManValidationException)e.getCause());
		} else {
			return buildErrorResponse(errMessage);
		}
	}
	
	@ExceptionHandler
	public ResponseEntity<ErrorResponseDTO<?>> handleException(TaskManValidationException e) {
		String errMsg = e.getMessage() == null ? e.getClass().getSimpleName() : e.getMessage();
		LOGGER.log(Level.SEVERE, errMsg, e);
		if(!e.getFieldErrors().isEmpty()) {
			return buildErrorResponse(e.getFieldErrors());
		} else {
			return buildErrorResponse(errMsg);
		}
	}
	
	private ResponseEntity<ErrorResponseDTO<?>> buildErrorResponse(List<FieldError> fieldErrors) {
		ErrorResponseDTO<FieldError> errorDTO = new ErrorResponseDTO<>(fieldErrors.toArray(new FieldError[fieldErrors.size()]));
		return new ResponseEntity<>(errorDTO , HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler
	public ResponseEntity<ErrorResponseDTO<?>> handleException(Exception e) {
		LOGGER.log(Level.SEVERE, e.getMessage(), e);
		return buildErrorResponse(messageSource.getMessage("internal.error", null, null));
	}

	private ResponseEntity<ErrorResponseDTO<?>> buildErrorResponse(String errorMessage) {
		ErrorResponseDTO<String> errorDTO = new ErrorResponseDTO<>(errorMessage);
		return new ResponseEntity<>(errorDTO , HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
