package com.mindtree.learning.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mindtree.learning.entity.TaskEntity;

@Repository
public interface TaskRepository extends JpaRepository<TaskEntity,String> {
	
	List<TaskEntity> findAllByProjectPid(String pid);
	
}
