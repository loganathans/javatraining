package com.mindtree.learning.dto;

import lombok.Data;

@Data
public class EmployeeDTO {
	private String mid;
	private String name;
}
