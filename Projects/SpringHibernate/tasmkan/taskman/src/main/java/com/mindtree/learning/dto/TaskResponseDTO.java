package com.mindtree.learning.dto;

import java.time.LocalDate;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class TaskResponseDTO {
	@ApiModelProperty(required = false, hidden = true)
	private String tid;
	private String desc;
	private String pid;
	private LocalDate startDate;
	private LocalDate dueDate;
	private List<EmployeeDTO> employees;
}
