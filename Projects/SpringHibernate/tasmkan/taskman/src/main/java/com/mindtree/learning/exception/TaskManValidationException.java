package com.mindtree.learning.exception;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import com.mindtree.learning.dto.FieldError;

public class TaskManValidationException extends Exception {
	
	private List<FieldError> fieldErrors = new ArrayList<>();


	/**
	 * 
	 */
	private static final long serialVersionUID = 8862315955032018396L;

	public TaskManValidationException() {
	}

	public TaskManValidationException(String arg0) {
		super(arg0);
	}

	public TaskManValidationException(Throwable arg0) {
		super(arg0);
	}

	public TaskManValidationException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public TaskManValidationException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
	}
	
	public List<FieldError> getFieldErrors() {
		return Collections.unmodifiableList(fieldErrors);
	}
	
	public void addFieldError(FieldError... fieldErrors) {
		Stream.of(fieldErrors)
			.filter(err -> !this.fieldErrors.contains(err))
			.forEach(this.fieldErrors::add);
	}

}
