package com.mindtree.learning.spi;

import java.util.List;

import com.mindtree.learning.dto.EmployeeDTO;
import com.mindtree.learning.dto.ProjectDTO;
import com.mindtree.learning.dto.ResponseDTO;
import com.mindtree.learning.dto.TaskRequestDTO;
import com.mindtree.learning.dto.TaskResponseDTO;
import com.mindtree.learning.exception.TaskManAppException;

public interface TaskManService {
	
	ResponseDTO<List<ProjectDTO>> getProjects() throws TaskManAppException;
	
	ResponseDTO<List<EmployeeDTO>> getEmployees(String pid) throws TaskManAppException;
	
	ResponseDTO<String> addTask(TaskRequestDTO taskRequestDTO) throws TaskManAppException;
	
	ResponseDTO<List<TaskResponseDTO>> getTasks(String pid) throws TaskManAppException;

}
