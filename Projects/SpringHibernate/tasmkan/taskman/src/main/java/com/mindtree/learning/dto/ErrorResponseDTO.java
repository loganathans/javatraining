package com.mindtree.learning.dto;

import lombok.Data;

@Data
public class ErrorResponseDTO<T> {
	
	private T[] errors;
	
	@SafeVarargs
	public ErrorResponseDTO(T... errors) {
		this.errors = errors;
	}

}
