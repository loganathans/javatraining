package com.mindtree.learning.dto;

import lombok.Data;

@Data
public class ProjectDTO {
	private String pid;
	private String name;
}
