package com.mindtree.learning.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.mindtree.learning.dto.EmployeeDTO;
import com.mindtree.learning.dto.ProjectDTO;
import com.mindtree.learning.dto.ResponseDTO;
import com.mindtree.learning.dto.TaskRequestDTO;
import com.mindtree.learning.dto.TaskResponseDTO;
import com.mindtree.learning.exception.TaskManAppException;
import com.mindtree.learning.exception.TaskManValidationException;
import com.mindtree.learning.spi.TaskManService;

@RestController
public class TaskManController {
	
	@Autowired
	private TaskManService taskManService;
	
	@Autowired
	@Qualifier("taskDatesValidator")
	private Validator validator;
	
	@Autowired
	private MessageSource messageSource;
	
	@InitBinder
	private void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}
	
	@GetMapping(path="/projects" ,produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseDTO<List<ProjectDTO>> getProjects() throws TaskManAppException{
		return taskManService.getProjects();
	}
	
	@GetMapping(path="/employees/{pid}" ,produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseDTO<List<EmployeeDTO>> getEmployees(@PathVariable("pid") String pid) throws TaskManAppException{
		return taskManService.getEmployees(pid);
	}
	
	@PostMapping(path="/task", consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseDTO<String> addTask(@RequestBody @Valid  TaskRequestDTO taskRequestDTO, BindingResult bindingResult) throws TaskManAppException {
		try {
			checkErrors(bindingResult);
		} catch (TaskManValidationException e) {
			throw new TaskManAppException(messageSource.getMessage("data.validation.error", null, null), e);
		}
		return taskManService.addTask(taskRequestDTO);
	}

	private void checkErrors(BindingResult bindingResult) throws TaskManValidationException {
		if (bindingResult.hasErrors()) {
			List<com.mindtree.learning.dto.FieldError> validationErrors = new ArrayList<>();
			List<FieldError> fieldErrors = bindingResult.getFieldErrors();
			if (!fieldErrors.isEmpty()) {
				TaskManValidationException taskManValidationException = new TaskManValidationException();
				for (FieldError fe : fieldErrors) {
					validationErrors
							.add(new com.mindtree.learning.dto.FieldError(fe.getField(), messageSource.getMessage(fe.getCode(), null, null)));
					taskManValidationException.addFieldError(validationErrors
							.toArray(new com.mindtree.learning.dto.FieldError[validationErrors.size()]));
				}
				throw taskManValidationException;
			}
		}
	}
	
	@GetMapping(path= {"/tasks/{pid}"}, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseDTO<List<TaskResponseDTO>> getTasks(@PathVariable(value="pid", required=false) String pid) throws TaskManAppException {
		if(pid == null || pid.equalsIgnoreCase("all")) {
			return taskManService.getTasks(null);
		} else {
			return taskManService.getTasks(pid);
		}
	}

}
