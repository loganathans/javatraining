package com.mindtree.learning.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mindtree.learning.entity.ProjectEntity;

@Repository
public interface ProjectRepository extends JpaRepository<ProjectEntity,String> {
}
