package com.mindtree.learning.exception;

public class TaskManAppException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8808880810184144791L;

	public TaskManAppException() {
	}

	public TaskManAppException(String arg0) {
		super(arg0);
	}

	public TaskManAppException(Throwable arg0) {
		super(arg0);
	}

	public TaskManAppException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public TaskManAppException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
	}

}
