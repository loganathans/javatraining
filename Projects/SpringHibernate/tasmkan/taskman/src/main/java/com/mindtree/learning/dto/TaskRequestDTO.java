package com.mindtree.learning.dto;

import java.time.LocalDate;
import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class TaskRequestDTO {
	@ApiModelProperty(required = false, hidden = true)
	private String tid;
	
	@NotNull
	@NotEmpty
	@Size(max=30)
	private String desc;
	
	@NotNull
	@NotEmpty
	@Size(max=10)
	private String pid;
	
	@NotNull
	private LocalDate startDate;
	
	@NotNull
	private LocalDate dueDate;
	
	@NotNull
	@NotEmpty
	private List<String> employees;
}
