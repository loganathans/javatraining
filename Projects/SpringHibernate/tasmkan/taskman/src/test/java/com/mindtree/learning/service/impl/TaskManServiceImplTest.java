package com.mindtree.learning.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.lang.reflect.Field;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.MessageSource;
import org.springframework.test.context.junit4.SpringRunner;

import com.mindtree.learning.dto.EmployeeDTO;
import com.mindtree.learning.dto.ProjectDTO;
import com.mindtree.learning.dto.ResponseDTO;
import com.mindtree.learning.dto.TaskRequestDTO;
import com.mindtree.learning.dto.TaskResponseDTO;
import com.mindtree.learning.entity.EmployeeEntity;
import com.mindtree.learning.entity.ProjectEntity;
import com.mindtree.learning.entity.TaskEntity;
import com.mindtree.learning.exception.TaskManAppException;
import com.mindtree.learning.repo.EmployeeRepository;
import com.mindtree.learning.repo.ProjectRepository;
import com.mindtree.learning.repo.TaskRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TaskManServiceImplTest {
	
	@Mock
	private TaskRepository taskRepo;

	@Mock
	private ProjectRepository projectRepo;

	@Mock
	private EmployeeRepository employeeRepo;

	private ModelMapper modelMapper = new ModelMapper();

	@Mock
	private EntityManager entityManager;
	
	@Autowired
	private MessageSource messageSource;

	
	@InjectMocks
	private TaskManServiceImpl testSubject;

	@PostConstruct
	private void initialize() throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		setFieldValue(testSubject, "modelMapper", modelMapper);
		setFieldValue(testSubject, "messageSource", messageSource);
	}

	public static void setFieldValue(Object instance, String filedName, Object value) throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		Field declaredField = instance.getClass().getDeclaredField(filedName);
		declaredField.setAccessible(true);
		declaredField.set(instance, value );
	}

	@Test
	public void testAddTask() throws Exception {
		TaskRequestDTO taskRequestDTO = new TaskRequestDTO();
		taskRequestDTO.setTid("T001");
		taskRequestDTO.setDesc("Task 1");
		LocalDate startDate = LocalDate.of(2019, 4, 10);
		LocalDate dueDate = LocalDate.of(2019, 4, 12);
		taskRequestDTO.setStartDate(startDate);
		taskRequestDTO.setDueDate(dueDate);
		taskRequestDTO.setPid("P001");
		taskRequestDTO.setEmployees(new ArrayList<>());
		taskRequestDTO.getEmployees().add("M001");
		taskRequestDTO.getEmployees().add("M002");
		ResponseDTO<String> result;

		// default test
		ProjectEntity projectEntity1 = new ProjectEntity();
		projectEntity1.setPid("P001");
		projectEntity1.setName("Project1");
		when(entityManager.getReference(ProjectEntity.class, taskRequestDTO.getPid())).thenReturn(projectEntity1);
		EmployeeEntity employeeEntity1 = new EmployeeEntity();
		employeeEntity1.setMid("M001");
		employeeEntity1.setName("Emp1");
		EmployeeEntity employeeEntity2 = new EmployeeEntity();
		employeeEntity2.setMid("M002");
		employeeEntity2.setName("Emp2");
		when(entityManager.getReference(EmployeeEntity.class, "M001")).thenReturn(employeeEntity1);
		when(entityManager.getReference(EmployeeEntity.class, "M002")).thenReturn(employeeEntity2);
		
		boolean[] dtoIsNull = new boolean[1];
		
		when(taskRepo.save(Mockito.any(TaskEntity.class))).thenAnswer(invocation -> {
			TaskEntity entity = invocation.getArgument(0);
			
			if(!dtoIsNull[0]) {
				assertEquals(taskRequestDTO.getTid(), entity.getTid());
			}
			assertEquals(taskRequestDTO.getDesc(), entity.getDesc());
			assertEquals(taskRequestDTO.getPid(),entity.getProject().getPid());
			assertEquals(taskRequestDTO.getStartDate(), entity.getStartDate());
			assertEquals(taskRequestDTO.getDueDate(), entity.getDueDate());
			
			assertEquals(taskRequestDTO.getEmployees().size(), entity.getEmployees().size());

			ArrayList<EmployeeEntity> empEntityList = new ArrayList<>(entity.getEmployees());
			IntStream.range(0, entity.getEmployees().size()).forEach(j -> {
				assertEquals(taskRequestDTO.getEmployees().get(j), empEntityList.get(j).getMid());
			});
			
			return null;
			
		});
		
		dtoIsNull[0]= false;
		result = testSubject.addTask(taskRequestDTO);
		
		String message = messageSource.getMessage("task.added", null, null) + taskRequestDTO.getTid() + " - " + taskRequestDTO.getDesc();
		assertEquals(message, result.getData());
		
		// test 3 - with null tid
		taskRequestDTO.setTid(null);
		dtoIsNull[0]= true;
		result = testSubject.addTask(taskRequestDTO);
		String messagePrefix = messageSource.getMessage("task.added", null, null);
		assertTrue(result.getData().startsWith(messagePrefix));
		
	}
	
	@Test(expected = TaskManAppException.class)
	public void testAddTaskWithException() throws Exception {
		TaskRequestDTO taskRequestDTO = new TaskRequestDTO();
		taskRequestDTO.setTid("T001");
		taskRequestDTO.setDesc("Task 1");
		LocalDate startDate = LocalDate.of(2019, 4, 10);
		LocalDate dueDate = LocalDate.of(2019, 4, 12);
		taskRequestDTO.setStartDate(startDate);
		taskRequestDTO.setDueDate(dueDate);
		taskRequestDTO.setPid("P001");
		taskRequestDTO.setEmployees(new ArrayList<>());
		taskRequestDTO.getEmployees().add("M001");
		taskRequestDTO.getEmployees().add("M002");

		// default test
		ProjectEntity projectEntity1 = new ProjectEntity();
		projectEntity1.setPid("P001");
		projectEntity1.setName("Project1");
		when(entityManager.getReference(ProjectEntity.class, taskRequestDTO.getPid())).thenReturn(projectEntity1);
		EmployeeEntity employeeEntity1 = new EmployeeEntity();
		employeeEntity1.setMid("M001");
		employeeEntity1.setName("Emp1");
		EmployeeEntity employeeEntity2 = new EmployeeEntity();
		employeeEntity2.setMid("M002");
		employeeEntity2.setName("Emp2");
		when(entityManager.getReference(EmployeeEntity.class, "M001")).thenReturn(employeeEntity1);
		when(entityManager.getReference(EmployeeEntity.class, "M002")).thenReturn(employeeEntity2);
		
		
		when(taskRepo.save(Mockito.any(TaskEntity.class))).thenThrow(RuntimeException.class);
		
		testSubject.addTask(taskRequestDTO);
		

	}

	@Test
	public void testGetEmployees() throws Exception {
		String pid = "M001";
		ResponseDTO<List<EmployeeDTO>> result;
		
		List<EmployeeEntity> value = new ArrayList<>();
		EmployeeEntity employeeEntity1 = new EmployeeEntity();
		employeeEntity1.setMid("M001");
		employeeEntity1.setName("Emp1");
		value.add(employeeEntity1);
		EmployeeEntity employeeEntity2 = new EmployeeEntity();
		employeeEntity2.setMid("M002");
		employeeEntity2.setName("Emp2");
		value.add(employeeEntity2);
		
		Mockito.when(employeeRepo.findAllByProjectsPid(pid)).thenReturn(value);

		// default test
		result = testSubject.getEmployees(pid);
		
		assertEquals(value.size(), result.getData().size());

		IntStream.range(0, value.size()).forEach(i -> {
			assertEquals(value.get(i).getMid(), result.getData().get(i).getMid());
			assertEquals(value.get(i).getName(), result.getData().get(i).getName());
		});
	}
	
	@Test(expected = TaskManAppException.class)
	public void testGetEmployeesWithException() throws Exception {
		String pid = "M001";
		
		List<EmployeeEntity> value = new ArrayList<>();
		EmployeeEntity employeeEntity1 = new EmployeeEntity();
		employeeEntity1.setMid("M001");
		employeeEntity1.setName("Emp1");
		value.add(employeeEntity1);
		EmployeeEntity employeeEntity2 = new EmployeeEntity();
		employeeEntity2.setMid("M002");
		employeeEntity2.setName("Emp2");
		value.add(employeeEntity2);
		
		Mockito.when(employeeRepo.findAllByProjectsPid(pid)).thenThrow(RuntimeException.class);

		testSubject.getEmployees(pid);
		
	}

	@Test
	public void testGetProjects() throws Exception {
		ResponseDTO<List<ProjectDTO>> result;

		// default test
		List<ProjectEntity> value = new ArrayList<>();
		ProjectEntity projectEntity1 = new ProjectEntity();
		projectEntity1.setPid("P001");
		projectEntity1.setName("Project1");
		value.add(projectEntity1);
		
		ProjectEntity projectEntity2 = new ProjectEntity();
		projectEntity2.setPid("P002");
		projectEntity2.setName("Project2");
		value.add(projectEntity2);
		
		Mockito.when(projectRepo.findAll()).thenReturn(value);
		result = testSubject.getProjects();
		
		assertEquals(value.size(), result.getData().size());

		IntStream.range(0, value.size()).forEach(i -> {
			assertEquals(value.get(i).getPid(), result.getData().get(i).getPid());
			assertEquals(value.get(i).getName(), result.getData().get(i).getName());
		});
	}

	@Test(expected = TaskManAppException.class)
	public void testGetProjectsWithException() throws Exception {
		List<ProjectEntity> value = new ArrayList<>();
		ProjectEntity projectEntity1 = new ProjectEntity();
		projectEntity1.setPid("P001");
		projectEntity1.setName("Project1");
		value.add(projectEntity1);
		
		ProjectEntity projectEntity2 = new ProjectEntity();
		projectEntity2.setPid("P002");
		projectEntity2.setName("Project2");
		value.add(projectEntity2);
		
		Mockito.when(projectRepo.findAll()).thenThrow(RuntimeException.class);
		
		testSubject.getProjects();
		
	}

	@Test
	public void testGetTasks() throws Exception {
		String pid = "";

		// test 1
		pid = null;
		List<TaskEntity> value1 = new ArrayList<>();
		TaskEntity taskEntity1 = new TaskEntity();
		taskEntity1.setTid("T001");
		taskEntity1.setDesc("Task 1");
		ProjectEntity project1 = new ProjectEntity();
		project1.setPid("P001");
		project1.setName("Project 1");
		taskEntity1.setProject(project1);
		LocalDate startDate = LocalDate.of(2019, 4, 10);
		LocalDate dueDate = LocalDate.of(2019, 4, 12);
		taskEntity1.setStartDate(startDate);
		taskEntity1.setDueDate(dueDate);
		
		List<EmployeeEntity> empValue = new ArrayList<>();
		EmployeeEntity employeeEntity1 = new EmployeeEntity();
		employeeEntity1.setMid("M001");
		employeeEntity1.setName("Emp1");
		empValue.add(employeeEntity1);
		EmployeeEntity employeeEntity2 = new EmployeeEntity();
		employeeEntity2.setMid("M002");
		employeeEntity2.setName("Emp2");
		empValue.add(employeeEntity2);
		
		taskEntity1.getEmployees().add(employeeEntity1);
		taskEntity1.getEmployees().add(employeeEntity2);
		
		
		TaskEntity taskEntity2 = new TaskEntity();
		taskEntity2.setTid("T002");
		taskEntity2.setDesc("Task 2");
		ProjectEntity project2 = new ProjectEntity();
		project2.setPid("P002");
		project2.setName("Project 2");
		taskEntity2.setProject(project2);
		
		List<EmployeeEntity> empValue2 = new ArrayList<>();
		EmployeeEntity employeeEntity3 = new EmployeeEntity();
		employeeEntity3.setMid("M003");
		employeeEntity3.setName("Emp3");
		empValue2.add(employeeEntity3);
		EmployeeEntity employeeEntity4 = new EmployeeEntity();
		employeeEntity4.setMid("M004");
		employeeEntity4.setName("Emp4");
		empValue2.add(employeeEntity4);
		
		taskEntity2.getEmployees().add(employeeEntity3);
		taskEntity2.getEmployees().add(employeeEntity4);
		
		value1.add(taskEntity1);
		value1.add(taskEntity2);
		
		Mockito.when(taskRepo.findAll()).thenReturn(value1);
		ResponseDTO<List<TaskResponseDTO>> result1 = testSubject.getTasks(pid);
		
		assertEquals(value1.size(), result1.getData().size());

		IntStream.range(0, value1.size()).forEach(i -> {
			assertEquals(value1.get(i).getTid(), result1.getData().get(i).getTid());
			assertEquals(value1.get(i).getDesc(), result1.getData().get(i).getDesc());
			assertEquals(value1.get(i).getProject().getPid(), result1.getData().get(i).getPid());
			assertEquals(value1.get(i).getStartDate(), result1.getData().get(i).getStartDate());
			assertEquals(value1.get(i).getDueDate(), result1.getData().get(i).getDueDate());
			
			assertEquals(value1.get(i).getEmployees().size(), result1.getData().get(i).getEmployees().size());

			ArrayList<EmployeeEntity> empList = new ArrayList<>(value1.get(i).getEmployees());
			IntStream.range(0, value1.size()).forEach(j -> {
				assertEquals(empList.get(j).getMid(), result1.getData().get(i).getEmployees().get(j).getMid());
				assertEquals(empList.get(j).getName(), result1.getData().get(i).getEmployees().get(j).getName());
			});
		});

		// test 2
		pid = "P001";
		
		TaskEntity taskEntity3 = new TaskEntity();
		taskEntity3.setTid("T003");
		taskEntity3.setDesc("Task 3");
		taskEntity3.setProject(project1);
		
		List<EmployeeEntity> empValue3 = new ArrayList<>();
		EmployeeEntity employeeEntity5 = new EmployeeEntity();
		employeeEntity5.setMid("M005");
		employeeEntity5.setName("Emp5");
		empValue3.add(employeeEntity5);
		EmployeeEntity employeeEntity6 = new EmployeeEntity();
		employeeEntity6.setMid("M006");
		employeeEntity6.setName("Emp6");
		empValue3.add(employeeEntity6);
		
		taskEntity3.getEmployees().add(employeeEntity5);
		taskEntity3.getEmployees().add(employeeEntity6);
		
		List<TaskEntity> value2 = new ArrayList<>();
		value2.add(taskEntity1);
		value2.add(taskEntity3);
		
		Mockito.when(taskRepo.findAllByProjectPid(pid)).thenReturn(value2);
		ResponseDTO<List<TaskResponseDTO>> result2 = testSubject.getTasks(pid);

		assertEquals(value2.size(), result2.getData().size());

		IntStream.range(0, value2.size()).forEach(i -> {
			assertEquals(value2.get(i).getTid(), result2.getData().get(i).getTid());
			assertEquals(value2.get(i).getDesc(), result2.getData().get(i).getDesc());
			assertEquals(value2.get(i).getProject().getPid(), result2.getData().get(i).getPid());
			assertEquals(value2.get(i).getStartDate(), result2.getData().get(i).getStartDate());
			assertEquals(value2.get(i).getDueDate(), result2.getData().get(i).getDueDate());
			
			assertEquals(value2.get(i).getEmployees().size(), result2.getData().get(i).getEmployees().size());

			ArrayList<EmployeeEntity> empList = new ArrayList<>(value2.get(i).getEmployees());
			IntStream.range(0, value2.size()).forEach(j -> {
				assertEquals(empList.get(j).getMid(), result2.getData().get(i).getEmployees().get(j).getMid());
				assertEquals(empList.get(j).getName(), result2.getData().get(i).getEmployees().get(j).getName());
			});
		});

	}
	
	@Test(expected = TaskManAppException.class)
	public void testGetTasksWithException() throws Exception {
		String pid = "";

		// test 1
		pid = null;
		List<TaskEntity> value1 = new ArrayList<>();
		TaskEntity taskEntity1 = new TaskEntity();
		taskEntity1.setTid("T001");
		taskEntity1.setDesc("Task 1");
		ProjectEntity project1 = new ProjectEntity();
		project1.setPid("P001");
		project1.setName("Project 1");
		taskEntity1.setProject(project1);
		LocalDate startDate = LocalDate.of(2019, 4, 10);
		LocalDate dueDate = LocalDate.of(2019, 4, 12);
		taskEntity1.setStartDate(startDate);
		taskEntity1.setDueDate(dueDate);
		
		List<EmployeeEntity> empValue = new ArrayList<>();
		EmployeeEntity employeeEntity1 = new EmployeeEntity();
		employeeEntity1.setMid("M001");
		employeeEntity1.setName("Emp1");
		empValue.add(employeeEntity1);
		EmployeeEntity employeeEntity2 = new EmployeeEntity();
		employeeEntity2.setMid("M002");
		employeeEntity2.setName("Emp2");
		empValue.add(employeeEntity2);
		
		taskEntity1.getEmployees().add(employeeEntity1);
		taskEntity1.getEmployees().add(employeeEntity2);
		
		
		TaskEntity taskEntity2 = new TaskEntity();
		taskEntity2.setTid("T002");
		taskEntity2.setDesc("Task 2");
		ProjectEntity project2 = new ProjectEntity();
		project2.setPid("P002");
		project2.setName("Project 2");
		taskEntity2.setProject(project2);
		
		List<EmployeeEntity> empValue2 = new ArrayList<>();
		EmployeeEntity employeeEntity3 = new EmployeeEntity();
		employeeEntity3.setMid("M003");
		employeeEntity3.setName("Emp3");
		empValue2.add(employeeEntity3);
		EmployeeEntity employeeEntity4 = new EmployeeEntity();
		employeeEntity4.setMid("M004");
		employeeEntity4.setName("Emp4");
		empValue2.add(employeeEntity4);
		
		taskEntity2.getEmployees().add(employeeEntity3);
		taskEntity2.getEmployees().add(employeeEntity4);
		
		value1.add(taskEntity1);
		value1.add(taskEntity2);
		
		Mockito.when(taskRepo.findAll()).thenThrow(RuntimeException.class);
		testSubject.getTasks(pid);
		
	}
}