package com.mindtree.learning.validator;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;

import com.mindtree.learning.dto.EmployeeDTO;
import com.mindtree.learning.dto.TaskRequestDTO;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TaskDatesValidatorTest {

	@Autowired
	private TaskDatesValidator testSubject;

	@Test
	public void testValidate() throws Exception {

		TaskRequestDTO taskRequestDTO = new TaskRequestDTO();
		taskRequestDTO.setTid("T001");
		taskRequestDTO.setDesc("Task 1");
		LocalDate startDate = LocalDate.of(2019, 4, 10);
		LocalDate dueDate = LocalDate.of(2019, 4, 12);
		taskRequestDTO.setStartDate(startDate);
		taskRequestDTO.setDueDate(dueDate);
		taskRequestDTO.setPid("P001");
		taskRequestDTO.setEmployees(new ArrayList<>());
		taskRequestDTO.getEmployees().add("M001");
		taskRequestDTO.getEmployees().add("M002");

		Errors errors = new BeanPropertyBindingResult(taskRequestDTO, "taskRequestDTO");

		// default test
		testSubject.validate(taskRequestDTO, errors);

		assertFalse(errors.hasErrors());

	}

	@Test
	public void testValidateErrors() throws Exception {

		TaskRequestDTO taskRequestDTO = new TaskRequestDTO();
		taskRequestDTO.setDesc("");
		LocalDate startDate = LocalDate.of(2019, 4, 10);
		LocalDate dueDate = LocalDate.of(2019, 4, 12);
		taskRequestDTO.setStartDate(startDate);
		taskRequestDTO.setDueDate(dueDate);
		taskRequestDTO.setPid("P001");
		taskRequestDTO.setEmployees(new ArrayList<>());
		taskRequestDTO.getEmployees().add("M001");
		taskRequestDTO.getEmployees().add("M002");

		Errors errors = new BeanPropertyBindingResult(taskRequestDTO, "taskRequestDTO");
		testSubject.validate(taskRequestDTO, errors);
		assertTrue(
			 errors.getFieldErrors().stream().anyMatch(fieldError -> 
			 		fieldError.getField().equals("desc") &&
			 		fieldError.getCode().equals("NotEmpty"))
		);

		taskRequestDTO.setDesc(null);
		errors = new BeanPropertyBindingResult(taskRequestDTO, "taskRequestDTO");
		testSubject.validate(taskRequestDTO, errors);
		assertTrue(
			 errors.getFieldErrors().stream().anyMatch(fieldError -> 
			 		fieldError.getField().equals("desc") &&
			 		fieldError.getCode().equals("NotNull"))
		);

		taskRequestDTO.setDesc("Task 1");
		
		taskRequestDTO.setPid("");
		errors = new BeanPropertyBindingResult(taskRequestDTO, "taskRequestDTO");
		testSubject.validate(taskRequestDTO, errors);
		assertTrue(
			 errors.getFieldErrors().stream().anyMatch(fieldError -> 
			 		fieldError.getField().equals("pid") &&
			 		fieldError.getCode().equals("NotEmpty"))
		);
		
		taskRequestDTO.setPid(null);
		errors = new BeanPropertyBindingResult(taskRequestDTO, "taskRequestDTO");
		testSubject.validate(taskRequestDTO, errors);
		assertTrue(
			 errors.getFieldErrors().stream().anyMatch(fieldError -> 
			 		fieldError.getField().equals("pid") &&
			 		fieldError.getCode().equals("NotNull"))
		);

		taskRequestDTO.setPid("POO1");

		taskRequestDTO.setEmployees(Collections.emptyList());
		errors = new BeanPropertyBindingResult(taskRequestDTO, "taskRequestDTO");
		testSubject.validate(taskRequestDTO, errors);
		assertTrue(
			 errors.getFieldErrors().stream().anyMatch(fieldError -> 
			 		fieldError.getField().equals("employees") &&
			 		fieldError.getCode().equals("NotEmpty"))
		);
		
		taskRequestDTO.setEmployees(null);
		errors = new BeanPropertyBindingResult(taskRequestDTO, "taskRequestDTO");
		testSubject.validate(taskRequestDTO, errors);
		assertTrue(
			 errors.getFieldErrors().stream().anyMatch(fieldError -> 
			 		fieldError.getField().equals("employees") &&
			 		fieldError.getCode().equals("NotNull"))
		);
		
		
		taskRequestDTO.setEmployees(new ArrayList<>());
		taskRequestDTO.getEmployees().add("M001");
		taskRequestDTO.getEmployees().add("M002");
		
		taskRequestDTO.setStartDate(null);
		errors = new BeanPropertyBindingResult(taskRequestDTO, "taskRequestDTO");
		testSubject.validate(taskRequestDTO, errors);
		assertTrue(
			 errors.getFieldErrors().stream().anyMatch(fieldError -> 
			 		fieldError.getField().equals("startDate") &&
			 		fieldError.getCode().equals("NotNull"))
		);
		
		taskRequestDTO.setDueDate(null);
		errors = new BeanPropertyBindingResult(taskRequestDTO, "taskRequestDTO");
		testSubject.validate(taskRequestDTO, errors);
		assertTrue(
			 errors.getFieldErrors().stream().anyMatch(fieldError -> 
			 		fieldError.getField().equals("dueDate") &&
			 		fieldError.getCode().equals("NotNull"))
		);
		
		taskRequestDTO.setStartDate(dueDate);
		taskRequestDTO.setDueDate(startDate);
		errors = new BeanPropertyBindingResult(taskRequestDTO, "taskRequestDTO");
		testSubject.validate(taskRequestDTO, errors);
		assertTrue(
			 errors.getFieldErrors().stream().anyMatch(fieldError -> 
			 		fieldError.getField().equals("dueDate") &&
			 		fieldError.getCode().equals("duedate.error"))
		);
	}

	@Test
	public void testSupports() throws Exception {

		// default test
		assertTrue(testSubject.supports(TaskRequestDTO.class));
		assertFalse(testSubject.supports(EmployeeDTO.class));

	}
}