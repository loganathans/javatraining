package com.mindtree.learning.exception;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.mindtree.learning.dto.ErrorResponseDTO;
import com.mindtree.learning.dto.FieldError;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TaskmanExceptionHandlerTest {

	@Autowired
	private TaskmanExceptionHandler testSubject;

	@Test
	public void testHandleException_AppException() throws Exception {
		TaskManAppException e = new TaskManAppException("Internal Error");
		ResponseEntity<ErrorResponseDTO<?>> result;

		// default test
		result = testSubject.handleException(e);

		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, result.getStatusCode());
		assertEquals("Internal Error", (String) result.getBody().getErrors()[0]);

		// test - 2
		e = new TaskManAppException();
		result = testSubject.handleException(e);

		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, result.getStatusCode());
		assertEquals("Internal Error", (String) result.getBody().getErrors()[0]);

		// test - 3
		e = new TaskManAppException(new TaskManValidationException());
		result = testSubject.handleException(e);

		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, result.getStatusCode());
		assertEquals(TaskManValidationException.class.getSimpleName(), (String) result.getBody().getErrors()[0]);

		// test - 4
		TaskManValidationException validationException = new TaskManValidationException("Validation Error");
		FieldError fieldError = new FieldError("dueDate", "Duedate should not be before startDate");
		validationException.addFieldError(fieldError);
		e = new TaskManAppException(validationException);
		result = testSubject.handleException(e);

		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, result.getStatusCode());
		assertEquals(fieldError, (FieldError) result.getBody().getErrors()[0]);
	}

	@Test
	public void testHandleException_ValidationException() throws Exception {
		TaskManValidationException e = null;
		ResponseEntity<ErrorResponseDTO<?>> result;

		// test - 1
		e = new TaskManValidationException();
		result = testSubject.handleException(e);

		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, result.getStatusCode());
		assertEquals(TaskManValidationException.class.getSimpleName(), (String) result.getBody().getErrors()[0]);

		// test - 2
		e = new TaskManValidationException("Validation Error");
		FieldError fieldError = new FieldError("dueDate", "Duedate should not be before startDate");
		e.addFieldError(fieldError);
		result = testSubject.handleException(e);

		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, result.getStatusCode());
		assertEquals(fieldError, (FieldError) result.getBody().getErrors()[0]);
	}

	@Test
	public void testHandleException_Exception() throws Exception {
		Exception e = new Exception();
		ResponseEntity<ErrorResponseDTO<?>> result;

		// default test
		result = testSubject.handleException(e);
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, result.getStatusCode());
		assertEquals("Internal Error", (String) result.getBody().getErrors()[0]);
	}
}