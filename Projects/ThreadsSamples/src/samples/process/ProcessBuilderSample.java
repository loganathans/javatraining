package samples.process;

import java.util.HashMap;

public class ProcessBuilderSample {
    public static void main(String[] args) {
        String commandArgs[] = new String[] { "java" };
        try {
            HashMap<String, String> envMap = new HashMap<String, String>();
            envMap.put("DS_HOME", "D:\\DS\\HOME");
            String myPath = "D:\\My\\path";
            String path = envMap.get("PATH");
            path = path == null ? myPath : (path + ";" + myPath);
            envMap.put("PATH", path);
            CommandLauncher.launch(commandArgs, envMap);

            commandArgs = new String[] { "notepad.exe" };
            CommandLauncher.launch(commandArgs, envMap);

            commandArgs = new String[] { "dir" };
            CommandLauncher.launch(commandArgs, envMap);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
