package samples.process;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.Map;
import java.util.Set;

public class CommandLauncher {

    public static void launch(final String[] commandArray, final Map<String, String> envMap) throws Exception {

        // Create Process builder and set env variables
        ProcessBuilder processBuilder = new ProcessBuilder(commandArray);
        if (envMap != null && !envMap.isEmpty()) {
            Map<String, String> processEnvMap = processBuilder.environment();
            Set<String> keySet = envMap.keySet();
            for (String key : keySet) {
                processEnvMap.put(key, envMap.get(key));
            }
        }

        // Start process
        final Process process = processBuilder.start();

        // Log the stream to console
        new Thread(new MessageConsoleWriter(process.getInputStream(), System.out)).start();
        new Thread(new MessageConsoleWriter(process.getInputStream(), System.err)).start();

        // Wait till the process finishes
        int exitValue = process.waitFor();

        System.out.println("Application exited with code: " + exitValue);

    }

    /**
     * The Message console writer implementation in a thread
     *
     */
    static class MessageConsoleWriter implements Runnable {
        private final InputStream from;
        private final PrintStream printStream;

        private MessageConsoleWriter(final InputStream from, final PrintStream printStream) {
            this.from = from;
            this.printStream = printStream;
        }

        @Override
        public void run() {
            BufferedReader reader = new BufferedReader(new InputStreamReader(from));
            String output = null;
            try {
                while ((output = reader.readLine()) != null) {
                    printStream.println(output);
                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

    }
}