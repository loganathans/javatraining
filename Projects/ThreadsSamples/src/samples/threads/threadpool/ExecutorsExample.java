package samples.threads.threadpool;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class ExecutorsExample {
	public static void main(String[] args) throws InterruptedException {
		ForkJoinPool workStealingPool = ForkJoinPool.commonPool();
		AtomicInteger intVal = new AtomicInteger();
		List<ForkJoinTask<Integer>> results = new ArrayList<>();
		for (int i = 0; i < 10; i++) {
			ForkJoinTask<Integer> future = workStealingPool.submit(intVal::incrementAndGet);
			results.add(future);
		}
		workStealingPool.awaitTermination(10, TimeUnit.SECONDS);
		
		results.forEach(future -> System.out.println(getVal(future)));
	}

	private static Integer getVal(ForkJoinTask<Integer> future) {
		try {
			return future.get();
		} catch (InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
