package samples.threads.timer;

import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Simple demo that uses java.util.Timer to schedule a task to execute once 5 seconds have passed.
 */

public class Reminder {
    static Timer timer = new Timer();

    public Reminder(int seconds) {
        System.out.println("Task scheduled:" + seconds + " seconds");
        timer.schedule(new RemindTask(1 + ""), seconds * 1000);
    }

    public Reminder(Date schedule) {
        System.out.println("Task scheduled at:" + schedule.toString());
        timer.schedule(new RemindTask(2 + ""), schedule);
    }

    public Reminder(long initialDelay, long interval) {
        System.out.println("Task scheduled in " + initialDelay + " seconds with interval " + interval);
        timer.schedule(new RemindTask(3 + ""), initialDelay * 1000, interval * 1000);
    }

    class RemindTask extends TimerTask {
        String name;

        RemindTask(String name) {
            this.name = name;
        }

        @Override
        public void run() {
            System.out.format("Task " + name + " completed%n");
        }
    }

    public static void main(String args[]) {
        // Timer task 1
        new Reminder(5);

        // Timer task 2
        // Get the Date corresponding to 11:01:00 pm today.
        Calendar calendar = Calendar.getInstance();
        // calendar.set(Calendar.HOUR_OF_DAY, 20);
        // calendar.set(Calendar.MINUTE, 20);
        // calendar.set(Calendar.SECOND, 0);
        calendar.setTime(new Date(System.currentTimeMillis() + 5 * 1000));
        final Date time = calendar.getTime();
        new Reminder(time);

        // Timer task 3
        final int initialDelay = 2;
        final int interval = 5;
        new Reminder(initialDelay, interval);

        // Put application on hold to see the tasks
        while (true) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                break;
            }
        }

        timer.cancel(); // Terminate the timer thread

    }
}