package samples.threads.sync;

import java.util.concurrent.atomic.AtomicInteger;

public class ThreadInterferenceFix {

    private static Counter counter;

    public static void main(final String[] args) {
        System.out.println("One thread increments, another decrements, any one should print 0.");
        counter = new Counter();
        Thread thread = new Thread() {
            @Override
            public void run() {
                for (int i = 0; i < 10000; i++) {
                    counter.increment();
                }
                System.out.println("Thread1 Result: " + counter.value());
            }
        };
        thread.start();
        new Thread() {
            @Override
            public void run() {
                for (int i = 0; i < 10000; i++) {
                    counter.decrement();
                }
                System.out.println("Thread2 Result: " + counter.value());
            }
        }.start();

    }

    static class Counter {
        private final AtomicInteger c = new AtomicInteger(0);

        public void increment() {
            c.incrementAndGet();
        }

        public void decrement() {
            c.decrementAndGet();
        }

        public int value() {
            return c.get();
        }
    }

}