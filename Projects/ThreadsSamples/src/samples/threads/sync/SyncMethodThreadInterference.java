package samples.threads.sync;

public class SyncMethodThreadInterference {

    private static Counter counter;

    public static void main(String[] args) {
        System.out.println("One thread increments, another decrements, any one should print 0.");
        counter = new Counter();
        new Thread() {
            @Override
            public void run() {
                for (int i = 0; i < 10000; i++) {
                    counter.increment();
                }
                System.out.println("Thread1 Result: " + counter.c);
            }
        }.start();

        new Thread() {
            @Override
            public void run() {
                for (int i = 0; i < 10000; i++) {
                    counter.decrement();
                }
                System.out.println("Thread2 Result: " + counter.c);
            }
        }.start();

    }

    static class Counter {
        private int c = 0;

        public synchronized void increment() {
            c++;
        }

        public synchronized void decrement() {
            c--;
        }

        public int value() {
            return c;
        }
    }

}