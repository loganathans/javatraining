package samples.threads.sync;

public class SyncStmtThreadInterference {

    private static Counter counter;

    public static void main(String[] args) {
        System.out.println("One thread increments, another decrements, any one should print 0.");
        counter = new Counter();
        new Thread() {
            @Override
            public void run() {
                for (int i = 0; i < 10000; i++) {
                    counter.increment();
                }
                System.out.println("Thread1 Result: " + counter.c);
            }
        }.start();

        new Thread() {
            @Override
            public void run() {
                for (int i = 0; i < 10000; i++) {
                    counter.decrement();
                }
                System.out.println("Thread2 Result: " + counter.c);
            }
        }.start();

    }

    static class Counter {
        Object lock = new Object();
        private int c = 0;

        public void increment() {
            synchronized (lock) {
                c++;
            }
        }

        public void decrement() {
            synchronized (lock) {
                c--;
            }
        }

        public int value() {
            return c;
        }
    }

}