package samples.threads.sync.guardedblocks;

public interface IDrop {

    public abstract String take();

    public abstract void put(String message);

}