package samples.threads.sync.guardedblocks;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class DropWithLockObject implements IDrop {

    final Lock lock = new ReentrantLock();
    final Condition notFull = lock.newCondition();
    final Condition notEmpty = lock.newCondition();

    // Message sent from producer
    // to consumer.
    private String message;
    // True if consumer should wait
    // for producer to send message,
    // false if producer should wait for
    // consumer to retrieve message.
    private boolean empty = true;

    /*
     * (non-Javadoc)
     * 
     * @see samples.threads.sync.guardedblocks.IDrop#take()
     */
    @Override
    public String take() {
        lock.lock();
        try {
            // Wait until message is
            // available.
            while (empty) {
                try {
                    notEmpty.await();
                } catch (InterruptedException e) {
                }
            }
            // Toggle status.
            empty = true;
            // Notify producer that
            // status has changed.
            notFull.signal();
        } finally {
            lock.unlock();
        }
        return message;
    }

    /*
     * (non-Javadoc)
     * 
     * @see samples.threads.sync.guardedblocks.IDrop#put(java.lang.String)
     */
    @Override
    public synchronized void put(String message) {
        lock.lock();
        try {
            // Wait until message has
            // been retrieved.
            while (!empty) {
                try {
                    notFull.await();
                } catch (InterruptedException e) {
                }
            }
            // Toggle status.
            empty = false;
            // Store message.
            this.message = message;
            // Notify consumer that status
            // has changed.
            notEmpty.signal();
        } finally {
            lock.unlock();
        }
    }
}