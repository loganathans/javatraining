package samples.threads.sync.guardedblocks;

public class ProducerConsumerExampleWithLockObject {
    public static void main(String[] args) {
        IDrop drop = new DropWithLockObject();
        (new Thread(new Producer(drop))).start();
        (new Thread(new Consumer(drop))).start();
    }
}