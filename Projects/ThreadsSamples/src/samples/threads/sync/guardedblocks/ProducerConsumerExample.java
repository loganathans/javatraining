package samples.threads.sync.guardedblocks;

public class ProducerConsumerExample {
    public static void main(String[] args) {
        IDrop drop = new Drop();
        (new Thread(new Producer(drop))).start();
        (new Thread(new Consumer(drop))).start();
    }
}