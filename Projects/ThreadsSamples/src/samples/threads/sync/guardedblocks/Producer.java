package samples.threads.sync.guardedblocks;

import java.util.Random;

public class Producer implements Runnable {
    private IDrop drop;

    public Producer(IDrop drop) {
        this.drop = drop;
    }

    @Override
    public void run() {
        String importantInfo[] = { "Mares eat oats", "Does eat oats", "Little lambs eat ivy", "A kid will eat ivy too" };
        Random random = new Random();

        for (String message : importantInfo) {
            drop.put(message);
            System.out.format("MESSAGE SENT:\t\t%s%n", message);
            try {
                Thread.sleep(random.nextInt(2000));
            } catch (InterruptedException e) {
            }
        }
        drop.put("DONE");
    }
}