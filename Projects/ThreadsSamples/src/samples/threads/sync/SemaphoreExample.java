package samples.threads.sync;

import java.util.Random;
import java.util.concurrent.Semaphore;

//Solving the mutual exclusion problem using Semaphore class

class SemaphoreExample extends Thread {
    private static final Random rand = new Random();

    private int id;

    private Semaphore sem;

    public SemaphoreExample(int i, Semaphore s) {
        id = i;
        sem = s;
    }

    private void busy() {
        try {
            sleep(rand.nextInt(500));
        } catch (InterruptedException e) {
        }
    }

    private void noncritical() {
        System.out.println("Thread " + id + " is NON critical");
        busy();
    }

    private void critical() {
        System.out.println("Thread " + id + " entering critical section");
        busy();
        System.out.println("Thread " + id + " leaving critical section");
    }

    @Override
    public void run() {
        for (int i = 0; i < 5; ++i) {
            noncritical();
            try {
                sem.acquire();
                System.out.println("Available permits after acquire: " + sem.availablePermits());
            } catch (InterruptedException e) {
                // �
            }
            critical();
            sem.release();
        }
    }

    public static void main(String[] args) {
        final int N = 4;

        System.out.println("Busy waiting�");

        // Semaphore(int permits, boolean fair)
        Semaphore sem = new Semaphore(N, true);
        SemaphoreExample[] p = new SemaphoreExample[N];

        for (int i = 0; i < N; i++) {
            p[i] = new SemaphoreExample(i, sem);
            p[i].start();
        }
    }
}